SisLog é um sistema desenvolvido para auxiliar o setor de logistica. Seu objetivo é promover a economia de tempo e dinheiro de motoristas autônomos, frotistas, transportadoras e embarcadores facilitando a comunicação dos mesmos em um único ambiente, buscando cargas, armazenagens e transportes.

Instalação

Faça o download dos arquivos  
Extraia o pacote e copie para seu webserver.  
Configure sua URL no arquivo config.php alterando a base_url.  
Crie o banco de dados e execute o arquivo banco.sql para criar as tabelas.  
Configure os dados de acesso ao banco de dados no arquivo database.php.  
Acesse sua URL e coloque os dados de acesso: admin e admin123.   
Requerimentos:  

PHP >= 5.4.0 
< 7.1  
MySQL  