<?php include 'includes/inc_header.php'; ?>
<?php include 'includes/inc_menuLateral.php'; ?>
<?php include 'includes/inc_menuSuperior.php'; ?>

<!DOCTYPE html>
<html lang="en">
    <body class="hold-transition skin-blue sidebar-mini">

        <?php if ($this->session->userdata('grupo') == '1' or $this->session->userdata('grupo') == '2'): ?>
            <?php include 'includes/inc_menuLateral.php'; ?>
        <div class="content-wrapper">
            <?php else:?>
            <?php redirect(''.base_url());?>
        <?php endif; ?>
            <div class="col-xs-8">
            </div>

            <div style="padding: 3px" class="col-sm-2" data-toggle="modal" data-target="#modal-cliente">
                <div class="btn btn-success btn-sm">
                    <span class="glyphicon glyphicon-new-window"></span> Novo Registro</div>
            </div>

            <table class=" table table-responsive ">
                <thead>
                    <tr>
                        <th>Código</th>
                        <th>Nome</th>
                        <th>CPF/CNPJ</th>
                        <th>Telefone</th>
                        <th>Cidade</th>
                        <th>Endereço</th>
                        <th>Email</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody >    
                    <?php foreach ($clientes as $cliente) { ?>
                        <tr>
                            <td> <?= $cliente->PESSOA_ID ?> </td>  
                            <td> <?= $cliente->NOME ?> </td>
                            <td> <?= $cliente->CPFCNPJ ?> </td>
                            <td> <?= $cliente->TELEFONE ?> </td>
                            <td> <?= $cliente->CIDADE_ID ?> </td>
                            <td> <?= $cliente->ENDERECO ?> </td>
                            <td> <?= $cliente->EMAIL ?> </td>
                            <td> 
                                <a  href="<?= base_url() . 'clientes/alterar/' . $cliente->PESSOA_ID ?>">
                                    Alterar
                                    <span class="glyphicon glyphicon-pencil" title="Alterar"></span>
                                </a> &nbsp;&nbsp;

                                <a href="<?= base_url() . 'clientes/del/' . $cliente->PESSOA_ID ?>"
                                   onclick="return confirm('Confirma Exclusão da Registro \'<?= $cliente->NOME ?>\'?')">
                                    Excluir
                                    <span class="glyphicon glyphicon-remove" title="Excluir"></span>
                                </a>
                            </td>
                        </tr>    
                    <?php } ?>

                </tbody>
            </table> 

           

            <div class="modal fade" id="modal-cliente">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Cadastro de Cliente</h4>
                        </div>
                        <div class="modal-body">
                            <form class="form-horizontal" action="clientes/grava_inclusao" method="POST" enctype="multipart/form-data">
                                <fieldset>
                                    <div class="col-sm-12 form-group margin50">
                                        <label class="col-lg-2"  for="NOME">Cliente</label>
                                        <div class="col-lg-4">
                                            <input type="text" id="NOME" name="NOME" placeholder="" class="form-control name" required="true">
                                        </div>
                                    </div>
                                    <div class="col-lg-12 form-group margin50">
                                        <label class="col-lg-2"  for="CPFCNPJ">CNPJ/CPF</label>
                                        <div class="col-lg-4">
                                            <input type="text" id="CPFCNPJ" name="CPFCNPJ" placeholder="" class="form-control name" required="true">
                                        </div>
                                    </div>
                                    <div class="col-lg-12 form-group margin50">
                                        <label class="col-lg-2"  for="TELEFONE">Telefone</label>
                                        <div class="col-lg-4">
                                            <input type="text" id="TELEFONE" name="TELEFONE" placeholder="" class="form-control name" required="true">
                                        </div>
                                    </div>
                                    <div class="col-lg-12 form-group margin50">
                                        <label class="col-lg-2"  for="CIDADE_ID">Cidade</label>
                                        <div class="col-lg-4">
                                            <input type="text" id="CIDADE_ID" name="CIDADE_ID" placeholder="" class="form-control name" required="true">
                                        </div>
                                    </div>
                                    <div class="col-lg-12 form-group margin50">
                                        <label class="col-lg-2"  for="ENDERECO">Endereço</label>
                                        <div class="col-lg-4">
                                            <input type="text" id="ENDERECO" name="ENDERECO" placeholder="" class="form-control name" required="true">
                                        </div>
                                    </div>
                                    <div class="col-lg-12 form-group margin50">
                                        <label class="col-lg-2"  for="EMAIL">Email</label>
                                        <div class="col-lg-4">
                                            <input type="text" id="EMAIL" name="EMAIL" placeholder="" class="form-control name" required="true">
                                        </div>
                                    </div>

                                </fieldset>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Fechar</button>
                                    <button type="submit" class="btn btn-primary">Salvar</button>
                                    <button type="reset" class="btn btn-default">Limpar</button>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </body>
</html>