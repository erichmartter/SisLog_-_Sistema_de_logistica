<!--Include cabeçalho-->
<?php include 'includes/inc_menuSuperior.php'; ?>
<?php include 'includes/inc_header.php'; ?>

<!DOCTYPE html>
<html lang="en">




    <body class="hold-transition skin-blue sidebar-mini">
        
        <?php if ($this->session->userdata('grupo') == '1' or $this->session->userdata('grupo') == '2'): ?>
            <?php include 'includes/inc_menuLateral.php'; ?>
        <div class="content-wrapper">
            <?php else:?>
            <?php redirect(''.base_url());?>
        <?php endif; ?>

            <div class="col-xs-8">

            </div>


            <div style="padding: 3px" class="col-sm-2" data-toggle="modal" data-target="#modal-produto">
                <div class="btn btn-success btn-sm">
                    <span class="glyphicon glyphicon-new-window"></span> Novo Produto</div>
            </div>

            <table class=" table table-responsive ">
                <thead>
                    <tr>
                        <th>Código</th>
                        <th>Produto</th>
                        <th>Categoria</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody >    
                    <?php foreach ($produtos as $produto) { ?>
                        <tr>
                            <td> <?= $produto->PRODUTO_ID ?> </td>  
                            <td> <?= $produto->DESC_PRODUTO ?> </td>
                            <td> <?= $produto->CATEGORIA ?> </td>
                            <td> 
                                <a  href="<?= base_url() . 'produtos/alterar/' . $produto->PRODUTO_ID ?>">
                                    Alterar
                                    <span class="glyphicon glyphicon-pencil" title="Alterar"></span>
                                </a> &nbsp;&nbsp;

                                <a href="<?= base_url() . 'produtos/del/' . $produto->PRODUTO_ID ?>"
                                   onclick="return confirm('Confirma Exclusão do Produto \'<?= $produto->DESC_PRODUTO ?>\'?')">
                                    Excluir
                                    <span class="glyphicon glyphicon-remove" title="Excluir"></span>
                                </a>
                            </td>
                        </tr>    
                    <?php } ?>

                </tbody>
            </table> 

                
            <div class="modal fade" id="modal-produto">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Cadastro de Produto</h4>
                        </div>
                        <div class="modal-body">
                            <form class="form-horizontal" action="produtos/grava_inclusao" method="POST" enctype="multipart/form-data">
                                <fieldset>
                                    <div class="col-lg-12 form-group margin50">
                                        <label class="col-lg-2"  for="DESC_PRODUTO">Produto</label>
                                        <div class="col-lg-4">
                                            <input type="text" id="nome" name="DESC_PRODUTO" placeholder="" class="form-control name" required="true">
                                        </div>
                                    </div>

                                    <div class=" col-lg-12 form-group">
                                        <label class="col-lg-2" for="CATEGORIA">Categoria</label>
                                        <div class="col-lg-4">
                                            <input type="text" id="CATEGORIA" name="CATEGORIA" placeholder="" class="form-control name" required="true">
                                        </div>

                                    </div>
                                    <div class=" col-lg-12 form-group">
                                        <label class="col-lg-2" for="SETOR">Setor</label>
                                        <div class="col-lg-4">
                                            <input type="text" id="SETOR" name="SETOR" placeholder="" class="form-control name" required="true">
                                        </div>

                                    </div>
                                    
                                </fieldset>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Fechar</button>
                                    <button type="submit" class="btn btn-primary">Salvar</button>
                                    <button type="reset" class="btn btn-default">Limpar</button>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </body>
</html>
