<!DOCTYPE html>
<html lang="en">
    <?php include 'includes/inc_header.php'; ?>
    <?php include 'includes/inc_menuSuperior.php'; ?>

    <body class="hold-transition skin-blue sidebar-mini">

        <?php if ($this->session->userdata('grupo') == '1' or $this->session->userdata('grupo') == '2'): ?>
            <?php include 'includes/inc_menuLateral.php'; ?>
            <div class="content-wrapper">
            <?php else:?>
            <?php redirect(''.base_url());?>
        <?php endif; ?>

            <div class="col-xs-8">
            </div>


            <div style="padding: 3px" class="col-sm-2" data-toggle="modal" data-target="#modal-veiculo">
                <div class="btn btn-success btn-sm">
                    <span class="glyphicon glyphicon-new-window"></span> Novo Veículo</div>
            </div>

            <table class="table table-responsive">
                <thead>
                    <tr>
                        <th>Código</th>
                        <th>Modelo do Veículo</th>
                        <th>Placa</th>
                        <th>Marca</th>

                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>    
                    <?php foreach ($veiculos as $veiculo) { ?>
                        <tr>
                            <td> <?= $veiculo->ID_VEICULO ?> </td>  
                            <td> <?= $veiculo->DESC_VEICULO ?> </td>
                            <td> <?= $veiculo->PLACA ?> </td>
                            <td> <?= $veiculo->DESC_MARCA ?> </td>

                            <td> 
                                <a href="<?= base_url() . 'veiculos/alterar/' . $veiculo->ID_VEICULO ?>">

                                    <span class="glyphicon glyphicon-pencil" title="Alterar"></span></a> &nbsp;&nbsp;
                                <a href="<?= base_url() . 'veiculos/del/' . $veiculo->ID_VEICULO ?>"
                                   onclick="return confirm('Confirma Exclusão do Veículo \'<?= $veiculo->DESC_VEICULO ?>\'?')">
                                    <span class="glyphicon glyphicon-remove" title="Excluir"></span></a>
                            </td>
                        </tr>    
                    <?php } ?>
                </tbody>
            </table>    
        </div>




        <div class="modal fade" id="modal-veiculo">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Cadastro de Veículo</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal" action="veiculos/grava_inclusao" method="POST" enctype="multipart/form-data">
                            <fieldset>

                                <div class="col-lg-12 form-group margin50">
                                    <label class="col-lg-2"  for="DESC_VEICULO">Veículo</label>
                                    <div class="col-lg-4">
                                        <input type="text" id="DESC_VEICULO" name="DESC_VEICULO" placeholder="" class="form-control name" required="true">
                                    </div>
                                </div>
                                <div class="col-lg-12 form-group margin50">
                                    <label class="col-lg-2"  for="PLACA">Placa</label>
                                    <div class="col-lg-4">
                                        <input type="text" id="PLACA" name="PLACA" placeholder="" class="form-control name" required="true">
                                    </div>
                                </div>
                                <div class="col-lg-12 form-group margin50">
                                    <label class="col-lg-2"  for="MARCA">Marca</label>
                                    <div class="col-lg-4">
                                        <input type="text" id="MARCA" name="MARCA" placeholder="" class="form-control name" required="true">
                                    </div>
                                </div>


                            </fieldset>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Fechar</button>
                                <button type="submit" class="btn btn-primary">Salvar</button>
                                <button type="reset" class="btn btn-default">Limpar</button>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>