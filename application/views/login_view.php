<!DOCTYPE html>
<!--Include cabeçalho-->

<!--Include do header-->
<?php include 'includes/inc_header.php'; ?>

<html>
    <body class="hold-transition login-page">
        <div class="col-md-3"><?= $this->session->flashdata('error') ?></div>
                
        

        <div class="login-box">
            <div class="login-logo">
                <a href=""><b>Sis</b>Log</a>
            </div>
            <!-- /.login-logo -->
            <div class="login-box-body">
                <p class="login-box-msg">Entre para iniciar sua sessão</p>
                <?= validation_errors() ?>
                
                <?= form_open('login') ?>



                    <div class="form-group has-feedback">
                        <input type="text" id="nome" name="nome" class="form-control" placeholder="Usuário">
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input id="senha" type="password" name="senha" class="form-control" placeholder="Senha">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="row">
                        <div class="col-xs-8">
                            
                        </div>
                        <!-- /.col -->
                        <div class="col-xs-4">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Entrar</button>
                            
                        </div>
                        <!-- /.col -->
                    </div>
                    <?php
                    echo form_close();
                    ?>


                    <a href="#">Esqueci a senha</a><br>
                    <a href="Registro" class="text-center">Criar conta</a>

                    <!--<a href="register.html" class="text-center">Register a new membership</a>-->
                    <?= form_close() ?>

                </div>
                <!-- /.login-box-body -->
            </div>

    </body>
</html>