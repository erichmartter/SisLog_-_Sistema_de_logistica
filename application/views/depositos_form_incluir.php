<!DOCTYPE html>
<html lang="en">

    <?php if ($this->session->userdata('grupo') == '3' || '1'): ?>
        <?php include 'includes/inc_menuSuperior_front.php'; ?>
    <?php endif; ?>
    <?php include 'includes/inc_header_front.php'; ?>



    <body>

        <div class="container-fluid">
            <!-- /.row -->
            <div class="row">
                <!-- body items -->

                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>
                                <i class="fa fa-fw fa-compass"></i> Cadastrar Anúncio
                            </h4>
                        </div><!-- /..panel-heading -->
                        <div class="panel-body">
                            <div><?= validation_errors() ?></div>
                            <?= form_open_multipart('anuncios/create', ['class' => 'form-group']) ?>

                            <div class="col-sm-4">
                                <div class="input-group">
                                    <div class="input-group-addon">Produto(s)</div>
                                    <input type="text" class="form-control" name="NOME" placeholder="Produto(s)" value="<?= set_value('NOME') ?>">
                                </div>
                            </div>

                            <div>
                                <div class="input-group-addon">Descrição</div>
                                <div class="col-sm-4">
                                    <div class="input-group col-sm-12">
                                        <textarea rows="4" class="form-control" name="DESCRICAO" placeholder="Descrição do anúncio"><?= set_value('DESCRICAO') ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12"><hr></div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <div class="input-group-addon">Valor sugerido</div>
                                    <input type="text" id="VALOR" class="form-control" name="VALOR" placeholder="Valor sugerido" value="<?= set_value('VALOR') ?>">
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="input-group">
                                    <div class="input-group-addon">Altura:</div>
                                    <input type="text" class="form-control" placeholder="Ex.: 10.5" name="ALTURA" value="<?= set_value('ALTURA') ?>">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <div class="input-group-addon">Largura:</div>
                                    <input type="text" class="form-control" placeholder="Ex.: 10.5" name="LARGURA" value="<?= set_value('LARGURA') ?>">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <div class="input-group-addon">Profundidade:</div>
                                    <input type="text" class="form-control" placeholder="Ex.: 10.5" name="PROFUNDIDADE" value="<?= set_value('PROFUNDIDADE') ?>">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <div class="input-group-addon">Peso:</div>
                                    <input type="text" class="form-control" placeholder="Kg" name="PESO" value="<?= set_value('PESO') ?>">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <div class="input-group-addon">Data da entrega:</div>
                                    <input type="date" class="form-control"  name="PRAZO_ENTREGA" value="<?= set_value('PRAZO_ENTREGA') ?>">
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="input-group">
                                    <input type="file" name="userfile">
                                </div>
                            </div>

                            <div class="col-sm-1">
                                <div class="input-group">
                                    <button type="submit" class="btn btn-primary">Create</button>
                                </div>
                            </div>
                            <div class="col-sm-1">
                                <div class="input-group">

                                    <?= anchor('home', 'Cancel', ['class' => 'btn btn-danger']) ?>
                                </div>
                            </div>


                            <?= form_close() ?>
                        </div><!-- /..panel-body -->
                    </div><!-- /..panel panel-default -->
                </div> 

            </div>

            <hr>
            <!-- Footer -->
            <?php $this->load->view('layout/footer') ?>

        </div>
        <!-- /.. container -->

        <!-- jQuery -->
        <script src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>
