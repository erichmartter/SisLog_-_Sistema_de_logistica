<?php include 'includes/inc_header.php'; ?>
<?php include 'includes/inc_menuSuperior.php'; ?>

<!DOCTYPE html>
<html lang="en">
    <body class="hold-transition skin-blue sidebar-mini">
        

        <?php if ($this->session->userdata('grupo') == '1' or $this->session->userdata('grupo') == '2'): ?>
            <?php include 'includes/inc_menuLateral.php'; ?>
        <div class="content-wrapper">
            <?php else:?>
            <?php redirect(''.base_url());?>
        <?php endif; ?>
            
            <div style="padding: 3px" class="col-sm-2" data-toggle="modal" data-target="#modal-marca">
                <div class="btn btn-success btn-sm">
                    <span class="glyphicon glyphicon-new-window"></span> Nova marca</div>
            </div>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Código</th>
                        <th>Marca</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>    
                    <?php foreach ($marcas as $marca) { ?>
                        <tr>
                            <td> <?= $marca->ID_MARCA ?> </td>  
                            <td> <?= $marca->DESC_MARCA ?> </td>
                            <td> 
                                <a href="<?= base_url() . 'marcas/alterar/' . $marca->ID_MARCA ?>">
                                    Alterar
                                    <span class="glyphicon glyphicon-pencil" title="Alterar"></span>
                                </a> &nbsp;&nbsp;

                                <a href="<?= base_url() . 'marcas/del/' . $marca->ID_MARCA ?>"
                                   onclick="return confirm('Confirma Exclusão da Marca \'<?= $marca->DESC_MARCA ?>\'?')">
                                    Excluir
                                    <span class="glyphicon glyphicon-remove" title="Excluir"></span>
                                </a>
                            </td>

                        </tr>    
                    <?php } ?>
                </tbody>
            </table> 

            <div class="modal fade" id="modal-marca">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Cadastro de Marca</h4>
                        </div>
                        <div class="modal-body">
                            <form class="form-horizontal" action="marcas/grava_inclusao" method="POST" enctype="multipart/form-data">
                                <fieldset>
                                    <div class="col-lg-12 form-group margin50">
                                        <label class="col-lg-2"  for="DESC_MARCA">Produto</label>
                                        <div class="col-lg-4">
                                            <input type="text" id="DESC_MARCA" name="DESC_MARCA" placeholder="" class="form-control name" required="true">
                                        </div>
                                    </div>

                                    <div class=" col-lg-12 form-group">
                                        <label class="col-lg-2" for="MARCA">Categoria</label>

                                    </div>
                                </fieldset>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Fechar</button>
                                    <button type="submit" class="btn btn-primary">Salvar</button>
                                    <button type="reset" class="btn btn-default">Limpar</button>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>




        </div>
    </body>
</html>