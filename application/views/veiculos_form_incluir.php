<!DOCTYPE html>
<!--Include cabeçalho-->
<?php include 'includes/inc_menuSuperior.php'; ?>
<!--Include do header-->
<?php include 'includes/inc_menuLateral.php'; ?>
<!--Include do header-->
<?php include 'includes/inc_header.php'; ?>
    <body>
        
        <div class="container">
           
            <form method="post" action="<?= base_url('veiculos/grava_inclusao') ?>"
                  enctype="multipart/form-data">

                <div class="col-sm-8">
                    <div class="form-group">
                        <label for="modelo"> <h4><br> Modelo do Veículo</h4> </label>
                        <input type="text" id="modelo" name="modelo" 
                               class="form-control" required>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="ano"> Ano </label>
                        <input type="text" id="ano" name="ano" 
                               class="form-control" required>
                    </div>
                </div>

                <div class="col-sm-8">
                    <div class="form-group">
                        <label for="marca"> Marca </label>
                        <select name="marca_id" id="marca" class="form-control" required>
                            <option value=""> Selecione... </option>
                            <?php foreach ($marcas as $marca) { ?>
                                <option value="<?= $marca->id ?>"> <?= $marca->nome ?> </option>                            
                            <?php } ?>
                        </select>
                    </div>
                </div>

               
                
                <div class="col-sm-12">
                    <button type="submit" class="btn btn-success">Enviar</button>
                    <button type="reset" class="btn btn-default">Limpar</button>
                </div>
            </form>
        </div>
    </body>
</html>
