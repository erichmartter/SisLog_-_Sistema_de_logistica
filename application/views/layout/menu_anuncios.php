
		<!-- Product Menu -->
		<div class="row">
            <div class="col-lg-12">
             <hr>
            </div>
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>Selecione uma categoria de anúncio </h4>
                    </div>
                    <div class="panel-body">
                        <p> 
							<?php foreach ($tipos as $tipo ) : ?>
							<!-- here to get name of product and show all ot same type -->
								<?=  anchor('home/showme/'.$tipo->TIPO_ID,$tipo->TIPO_DESC,['class'=>'btn btn-default']) ?>
                            <?php endforeach; ?>
                        </p>
                        <div class="text-right"><a><?php echo anchor('home', 'Limpar filtros'); ?></a> </div>
                           
                    </div>
                </div>
            </div> 
        </div>