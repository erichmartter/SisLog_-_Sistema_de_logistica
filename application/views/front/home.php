<!DOCTYPE HTML>
<html>
    <?php include 'includes/inc_header_front.php'; ?>
    <?php include 'includes/inc_menuSuperior_front.php'; ?>

    <body>
        <?php $this->load->view('layout/menu_anuncios')?>
        

        <div class="main">
            <div class="shop_top">
                <div class="container">
                    <div  class="row shop_box-top">
                        <?php foreach ($anuncios as $anuncio) : ?>
                            <div class="col-md-3 shop_box"><a href="anuncios/single"<?php $anuncio ?> ></a>
                                <?php
                                $anuncio_image = ['src' => 'assets/uploads/' . $anuncio->ANUNCIO_IMAGEM,
                                    'class' => 'img-responsive img-portfolio img-hover',
                                    'id' => 'g'
                                ];
                                echo img($anuncio_image);
                                ?>
                                <div class="shop_desc">
                                    <h3><a href="#"><?= $anuncio->DESCRICAO ?> </a></h3>
                                    <p>Lorem ipsum consectetuer adipiscing </p>
                                    <span class="actual"><?= $anuncio->VALOR ?></span><br>
                                    <?php if ($this->session->userdata('grupo') != '1' and $this->session->userdata('grupo') != '2'): ?>
                                        <?= anchor('anuncios/single/' . $anuncio->ANUNCIO_ID, 'Aderir serviço', ['class' => 'btn btn-success  btn-xs', 'role' => 'button']) ?>
                                        <?= anchor('home/add_to_cart/' . $anuncio->ANUNCIO_ID, 'Aderir serviço', ['class' => 'btn btn-success  btn-xs', 'role' => 'button']) ?>
                                        <?= anchor('home/report/' . $anuncio->ANUNCIO_ID, 'reportar', ['class' => 'btn btn-danger btn-xs', 'role' => 'button']) ?>

                                    <?php else: ?>
                                        <?= anchor('anuncios/edit/' . $anuncio->ANUNCIO_ID, 'Editar', ['class' => 'btn btn-success btn-xs']) ?>
                                        <?php if ($this->session->userdata('grupo') == '1'): ?>
                                            <?=
                                            anchor('anuncios/delete/' . $anuncio->ANUNCIO_ID, 'Deletar', ['class' => 'btn btn-danger btn-xs',
                                                'onclick' => 'return confirm(\'Are You Sure ? \')'
                                            ])
                                            ?>
                                        <?php else: ?>
                                            <?=
                                            anchor('admin/products/delete/', 'Delete', ['class' => 'btn btn-danger btn-xs ', 'data-toggle' => 'button',
                                                'onclick' => 'return confirm(\'Sorry You Cant Delete it , Should Be Admin ! \')'
                                            ])
                                            ?>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </div>


                            </div>
                        <?php endforeach; ?>

                    </div>
                </div>
            </div>
        </div>
        <div class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <ul class="footer_box">
                            <h4>Products</h4>
                            <li><a href="#">Mens</a></li>
                            <li><a href="#">Womens</a></li>
                            <li><a href="#">Youth</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <ul class="footer_box">
                            <h4>About</h4>
                            <li><a href="#">Careers and internships</a></li>
                            <li><a href="#">Sponserships</a></li>
                            <li><a href="#">team</a></li>
                            <li><a href="#">Catalog Request/Download</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <ul class="footer_box">
                            <h4>Customer Support</h4>
                            <li><a href="#">Contact Us</a></li>
                            <li><a href="#">Shipping and Order Tracking</a></li>
                            <li><a href="#">Easy Returns</a></li>
                            <li><a href="#">Warranty</a></li>
                            <li><a href="#">Replacement Binding Parts</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <ul class="footer_box">
                            <h4>Newsletter</h4>
                            <div class="footer_search">
                                <form>
                                    <input type="text" value="Enter your email" onfocus="this.value = '';" onblur="if (this.value == '') {
                                                this.value = 'Enter your email';
                                            }">
                                    <input type="submit" value="Go">
                                </form>
                            </div>
                            <ul class="social">	
                                <li class="facebook"><a href="#"><span> </span></a></li>
                                <li class="twitter"><a href="#"><span> </span></a></li>
                                <li class="instagram"><a href="#"><span> </span></a></li>	
                                <li class="pinterest"><a href="#"><span> </span></a></li>	
                                <li class="youtube"><a href="#"><span> </span></a></li>										  				
                            </ul>

                        </ul>
                    </div>
                </div>
                <div class="row footer_bottom">
                    <div class="copy">
                        <p>© 2014 Template by <a href="http://w3layouts.com" target="_blank">w3layouts</a></p>
                    </div>
                </div>
            </div>
        </div>
    </body>	
</html>