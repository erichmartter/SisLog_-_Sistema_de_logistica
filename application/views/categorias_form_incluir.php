<!DOCTYPE html>
<html lang="en">
    
    <?php include 'includes/inc_header.php'; ?>
    <?php include 'includes/inc_menuLateral.php'; ?>
    <?php include 'includes/inc_menuSuperior.php'; ?>
    <body>
        <div class="container">
           
            <form method="post" action="<?= base_url('categorias/grava_inclusao') ?>"
                  enctype="multipart/form-data">

                <div class="col-sm-8">
                    <div class="form-group">
                        <label for="modelo"><br> Categoria </label>
                        <input type="text" id="nome" name="nome" 
                               class="form-control" required>
                    </div>
                </div>

               
                
                <div class="col-sm-12">
                    <button type="submit" class="btn btn-success">Enviar</button>
                    <button type="reset" class="btn btn-default">Limpar</button>
                </div>    
            </form>
        </div>
    </body>
</html>
