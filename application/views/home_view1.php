


<?php include 'includes/inc_header.php'; ?>
<?php include 'includes/inc_menuSuperior.php'; ?>


<body class="hold-transition skin-blue sidebar-mini">
    <?php if ($this->session->userdata('grupo') == '1'): ?>
        <?php include 'includes/inc_menuLateral.php'; ?>
    <div class="content-wrapper">
    <?php endif; ?>
        <!-- Product Menu -->
        <?php $this->load->view('layout/menu_produtos') ?>
        <!-- /.row -->
        <div class="row">

            <!-- body items -->
            <!-- load products from table -->
            <?php foreach ($produtos as $produto) : ?>
                <div class="col-md-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">

                            <h6><?= $produto->PRODUTO_NOME ?> </h6>

                        </div>
                        <div class="panel-body" width="100px">
                            <a href="">
                                <style>#g {width:500%;height: 120px;}</style>
                                <?php
                                $produto_image = ['src' => 'assets/uploads/' . $produto->PRODUTO_IMAGEM,
                                    'class' => 'img-responsive img-portfolio img-hover',
                                    'id' => 'g'
                                ];
                                echo img($produto_image);
                                ?>
                            </a>
                            <style>#t {width: 230px;height: 75px;overflow: scroll;}</style>
                            <p id="t"> <?= $produto->DESC_PRODUTO ?></p>
                            <p><code>Price:</code> <?= $produto->VALOR ?>  <code> Stock:</code> <?= $produto->PRODUTO_ESTOQUE ?> </p>
                            <?php if ($this->session->userdata('group') != '1' and $this->session->userdata('group') != '2'): ?>
                                <?= anchor('home/add_to_cart/' . $produto->PRODUTO_ID, 'Add To Cart || Buy', ['class' => 'btn btn-success  btn-xs', 'role' => 'button']) ?>
                                <ul class="nav nav-tabs navbar-right">
                                    <li class="dropdown">
                                        <a class="dropdown-toggle" data-toggle="dropdown">Report <i class="fa fa-exclamation-triangle"></i></a> 
                                        <ul class="dropdown-menu">
                                            <li>
                                                <?= anchor('home/report/' . $produto->PRODUTO_ID, "I don't  like this Product", ["class'=>'btn  btn-xs"]) ?>
                                            </li>
                                        </ul>
                                    </li> 
                                </ul>

                            <?php else: ?>
                                <?= anchor('admin/products/edit/' . $produto->PRODUTO_ID, 'Edit', ['class' => 'btn btn-success btn-xs']) ?>
                                <?php if ($this->session->userdata('group') == '1'): ?>
                                    <?=
                                    anchor('admin/products/delete/' . $produto->PRODUTO_ID, 'Delete', ['class' => 'btn btn-danger btn-xs',
                                        'onclick' => 'return confirm(\'Are You Sure ? \')'
                                    ])
                                    ?>
                                <?php else: ?>
                                    <?=
                                    anchor('admin/products/delete/', 'Delete', ['class' => 'btn btn-danger btn-xs ', 'data-toggle' => 'button',
                                        'onclick' => 'return confirm(\'Sorry You Cant Delete it , Should Be Admin ! \')'
                                    ])
                                    ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>  
            <?php endforeach; ?>
        </div>

    </div>

</body>