<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<?php if ($this->session->userdata('grupo') == '3' || '1'): ?>
    <?php include 'includes/inc_menuSuperior_front.php'; ?>
<?php endif; ?>
<?php include 'includes/inc_header_front.php'; ?>
<html>
    <body>
        <div class="main">
            <div class="shop_top">
                <div class="container">
                    <div class="row">
                        <div class="col-md-9 single_left">
                            <div class="single_image">
                                <ul id="etalage">
                                    <li>
                                        <img class="etalage_thumb_image" src="../../assets/assetsFront/images/3.jpg" />
                                            <img class="etalage_source_image" src="images/3.jpg" />
                                    </li>
                                </ul>
                            </div>
                            <!-- end product_slider -->
                            <div class="single_right">
                                <h3> <?= $DESCRICAO ?>  </h3>
                                <h3>hendrerit in vulputate velit </h3>
                                <p class="m_10">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse</p>
                            </div>
                            <div class="clear"> </div>
                        </div>
                        <div class="col-md-3">
                            <div class="box-info-product">
                                <p class="price2">$130.25</p>
                                <button type="submit" name="Submit" class="exclusive">
                                    <span>Confirmar <br>serviço</span>
                                </button>
                            </div>
                        </div>
                    </div>		
                </div>
            </div>
        </div>
        <div class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <ul class="footer_box">
                            <h4>Products</h4>
                            <li><a href="#">Mens</a></li>
                            <li><a href="#">Womens</a></li>
                            <li><a href="#">Youth</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <ul class="footer_box">
                            <h4>About</h4>
                            <li><a href="#">Careers and internships</a></li>
                            <li><a href="#">Sponserships</a></li>
                            <li><a href="#">team</a></li>
                            <li><a href="#">Catalog Request/Download</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <ul class="footer_box">
                            <h4>Customer Support</h4>
                            <li><a href="#">Contact Us</a></li>
                            <li><a href="#">Shipping and Order Tracking</a></li>
                            <li><a href="#">Easy Returns</a></li>
                            <li><a href="#">Warranty</a></li>
                            <li><a href="#">Replacement Binding Parts</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <ul class="footer_box">
                            <h4>Newsletter</h4>
                            <div class="footer_search">
                                <form>
                                    <input type="text" value="Enter your email" onfocus="this.value = '';" onblur="if (this.value == '') {
                                                this.value = 'Enter your email';}">
                                    <input type="submit" value="Go">
                                </form>
                            </div>
                            <ul class="social">	
                                <li class="facebook"><a href="#"><span> </span></a></li>
                                <li class="twitter"><a href="#"><span> </span></a></li>
                                <li class="instagram"><a href="#"><span> </span></a></li>	
                                <li class="pinterest"><a href="#"><span> </span></a></li>	
                                <li class="youtube"><a href="#"><span> </span></a></li>										  				
                            </ul>
                        </ul>
                    </div>
                </div>
                <div class="row footer_bottom">
                    <div class="copy">
                        <p>© 2014 Template by <a href="http://w3layouts.com" target="_blank">w3layouts</a></p>
                    </div>
                    <dl id="sample" class="dropdown">
                        <dt><a href="#"><span>Change Region</span></a></dt>
                        <dd>
                            <ul>
                                <li><a href="#">Australia<img class="flag" src="images/as.png" alt="" /><span class="value">AS</span></a></li>
                                <li><a href="#">Sri Lanka<img class="flag" src="images/srl.png" alt="" /><span class="value">SL</span></a></li>
                                <li><a href="#">Newziland<img class="flag" src="images/nz.png" alt="" /><span class="value">NZ</span></a></li>
                                <li><a href="#">Pakistan<img class="flag" src="images/pk.png" alt="" /><span class="value">Pk</span></a></li>
                                <li><a href="#">United Kingdom<img class="flag" src="images/uk.png" alt="" /><span class="value">UK</span></a></li>
                                <li><a href="#">United States<img class="flag" src="images/us.png" alt="" /><span class="value">US</span></a></li>
                            </ul>
                        </dd>
                    </dl>
                </div>
            </div>
        </div>
    </body>	
</html>