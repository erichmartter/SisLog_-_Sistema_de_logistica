


<?php include 'includes/inc_header.php'; ?>
<?php include 'includes/inc_menuSuperior.php'; ?>


<body class="hold-transition skin-blue sidebar-mini">
    <?php if ($this->session->userdata('grupo') == '1'): ?>
        <?php include 'includes/inc_menuLateral.php'; ?>
    <div class="content-wrapper">
    <?php endif; ?>
        <!-- Product Menu -->
        <?php $this->load->view('layout/menu_anuncios') ?>
        <!-- /.row -->
        <div class="row">

            <!-- body items -->
            <!-- load products from table -->
            <?php foreach ($anuncios as $anuncio) : ?>
                <div class="col-md-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">

                            <h6><?= $anuncio->NOME ?> </h6>

                        </div>
                        <div class="panel-body" width="100px">
                            <a href="">
                                <style>#g {width:500%;height: 120px;}</style>
                                <?php
                                $anuncio_image = ['src' => 'assets/uploads/' . $anuncio->ANUNCIO_IMAGEM,
                                    'class' => 'img-responsive img-portfolio img-hover',
                                    'id' => 'g'
                                ];
                                echo img($anuncio_image);
                                ?>
                            </a>
                            <style>#t {width: 230px;height: 75px;overflow: scroll;}</style>
                            <p id="t"> <?= $anuncio->DESCRICAO ?></p>
                            <p><code>Preço sugerido:</code> <?= $anuncio->VALOR ?> </p>
                            <?php if ($this->session->userdata('group') != '1' and $this->session->userdata('group') != '2'): ?>
                                <?= anchor('home/add_to_cart/' . $anuncio->ANUNCIO_ID, 'Add To Cart || Buy', ['class' => 'btn btn-success  btn-xs', 'role' => 'button']) ?>
                                <ul class="nav nav-tabs navbar-right">
                                    <li class="dropdown">
                                        <a class="dropdown-toggle" data-toggle="dropdown">Report <i class="fa fa-exclamation-triangle"></i></a> 
                                        <ul class="dropdown-menu">
                                            <li>
                                                <?= anchor('home/report/' . $anuncio->ANUNCIO_ID, "I don't  like this Product", ["class'=>'btn  btn-xs"]) ?>
                                            </li>
                                        </ul>
                                    </li> 
                                </ul>

                            <?php else: ?>
                                <?= anchor('admin/products/edit/' . $anuncio->ANUNCIO_ID, 'Edit', ['class' => 'btn btn-success btn-xs']) ?>
                                <?php if ($this->session->userdata('group') == '1'): ?>
                                    <?=
                                    anchor('admin/products/delete/' . $anuncio->ANUNCIO_ID, 'Delete', ['class' => 'btn btn-danger btn-xs',
                                        'onclick' => 'return confirm(\'Are You Sure ? \')'
                                    ])
                                    ?>
                                <?php else: ?>
                                    <?=
                                    anchor('admin/products/delete/', 'Delete', ['class' => 'btn btn-danger btn-xs ', 'data-toggle' => 'button',
                                        'onclick' => 'return confirm(\'Sorry You Cant Delete it , Should Be Admin ! \')'
                                    ])
                                    ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>  
            <?php endforeach; ?>
        </div>

    </div>

</body>