
<!DOCTYPE html>
<!--Include cabeçalho-->

<!--Include do header-->
<?php include 'includes/inc_header.php'; ?>

<html>
    <body class="hold-transition login-page">
        <div class="col-md-3"><?= $this->session->flashdata('error') ?></div>

    <div class="login-box">
        <div class="login-logo">
            <a href=""><b>Sis</b>Log</a>
        </div>
        <div class="register-box">
            <div class="register-box-body">
                <p class="login-box-msg">Insira seus dados</p>


                <?= validation_errors() ?>
                <?= form_open('Registro') ?>
                <div for="rnome" class="form-group has-feedback">
                    <input name="rnome" type="text" class="form-control" placeholder="Nome completo/Razão Social" value="<?= set_value('rnome') ?>">
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>
                <div for="rsenha" class="form-group has-feedback">
                    <input type="password" class="form-control" name="rsenha" placeholder="Senha" value=" <?= set_value('rsenha') ?>">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div for="resenha" class="form-group has-feedback">
                    <input name="resenha" type="password" class="form-control" placeholder="Reinserir senha" value="<?= set_value('resenha') ?>">
                    <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                </div>
                <div class="row">
                    <div class="col-xs-8">
                        <div>
                            <label>
                                <input type="checkbox"> Eu aceito os <a data-toggle="modal" data-target="#modal-termos">termos</a> de uso
                            </label>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Cadastrar</button>
                    </div>
                    <!-- /.col -->
                </div>
                <?= form_close() ?>


                <a href="Login" class="text-center">Já tenho um cadastro</a>
            </div>
            <!-- /.form-box -->
        </div>
        <!-- /.register-box -->

    </div>


    <div class="modal fade" id="modal-termos">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Termos de Uso e Política de Privacidade</h4>
                </div>
                <div class="modal-body">
                    <p>
                        Passagem padrão original de Lorem Ipsum, usada desde o século XVI.

                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."

                        Seção 1.10.32 de "de Finibus Bonorum et Malorum", escrita por Cícero em 45 AC

                        "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"

                        Tradução para o inglês por H. Rackha, feita em 1914

                        "But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?"

                        Seção 1.10.33 de "de Finibus Bonorum et Malorum", escrita por Cícero em 45 AC

                        "At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat."
                    </p>
                </div>
            </div>
        </div>
    </div>
</body>
</html>