<?php include 'includes/inc_menuSuperior.php'; ?>
<?php include 'includes/inc_header.php'; ?>
<!DOCTYPE html>

<html lang="en">
    <!--Include cabeçalho-->

    <body class="hold-transition skin-blue sidebar-mini">

        <?php if ($this->session->userdata('grupo') == '1' or $this->session->userdata('grupo') == '2'): ?>
            <?php include 'includes/inc_menuLateral.php'; ?>
        <div class="content-wrapper">
            <?php else:?>
            <?php redirect(''.base_url());?>
        <?php endif; ?>

            <div class="col-xs-8">

            </div>
            <form method="post" action="<?= base_url('produtos/grava_alteracao') ?>"
                  enctype="multipart/form-data">

                <input type="hidden" name="id" value="<?= $produto->id ?>">

                <div class="col-sm-8">
                    <div class="form-group">
                        <label for="modelo"> <br> Marca </label>
                        <input type="text" id="DESC_PRODUTO" name="DESC_PRODUTO" 
                               value="<?= $produto->DESC_PRODUTO ?>"
                               class="form-control" required>
                    </div>

                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-success">Enviar</button>
                        <button type="reset" class="btn btn-default">Limpar</button>
                    </div>    

                </div>
            </form>

        </div>

    </body>
</html>
