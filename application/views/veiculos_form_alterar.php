<!DOCTYPE html>
<html lang="en">

    <?php include 'includes/inc_header.php'; ?>
    <?php include 'includes/inc_menuLateral.php'; ?>
    <?php include 'includes/inc_menuSuperior.php'; ?>
    
    <body class="hold-transition skin-blue sidebar-mini">



        <div class="content-wrapper">

            <form method="post" action="<?= base_url('veiculos/grava_alteracao') ?>"
                  enctype="multipart/form-data">

                <input type="hidden" name="ID_VEICULO" value="<?= $veiculo->ID_VEICULO ?>">

                <div class="col-sm-8">
                    <div class="form-group">
                        <label for="DESC_VEICULO"> <br>
                            Modelo do Veículo </label>
                        <input type="text" id="DESC_VEICULO" name="DESC_VEICULO" 
                               value="<?= $veiculo->DESC_VEICULO ?>"
                               class="form-control" required>
                    </div>
                </div>

                <div class="col-sm-8">
                    <div class="form-group">
                        <label for="PLACA"> <br>
                            Placa do Veículo </label>
                        <input type="text" id="PLACA" name="PLACA" 
                               value="<?= $veiculo->PLACA ?>"
                               class="form-control" required>
                    </div>
                </div>



                <div class="col-sm-8">
                    <div class="form-group">
                        <label for="marca"> Marca </label>
                        <select name="MARCA" id="ID_MARCA" class="form-control" required>
                            <option value=""> Selecione... </option>
                            <?php foreach ($marcas as $marca) { ?>
                                <option value="<?= $marca->ID_MARCA ?>"> <?= $marca->DESC_MARCA ?> </option>                            
                            
                                    <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-12">
                    <button type="submit" class="btn btn-success">Enviar</button>
                    <button type="reset" class="btn btn-default">Limpar</button>
                </div>    
            </form>
        </div>
    </body>
</html>
