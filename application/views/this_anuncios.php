<!DOCTYPE html>
<html lang="en">
    <?php include 'includes/inc_header.php'; ?>
    <?php include 'includes/inc_menuSuperior.php'; ?>


    <body>

        <!-- Navigation Top_Menu -->
        <?php $this->load->view('layout/navigation') ?>
        <!-- Header Carousel -->
        <!-- Page Content -->
        <div class="container">
            <!-- Product Menu -->
            <?php $this->load->view('layout/menu_anuncios') ?>
            <!-- /.row -->
            <div class="row">
                <!-- body items -->
                <!-- load products from table --><?php $send = 'add'; ?>
                <?php foreach ($comes as $come) : ?>
                    <div class="col-md-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h6><?= $come->TIPO_ANUNCIO ?></h6>
                            </div>
                            <div class="panel-body">
                                <style>#g {width:500%;height: 120px;}</style>
                                <?php
                                $product_image = ['src' => 'assets/uploads/' . $come->ANUNCIO_IMAGEM,
                                    'class' => 'img-responsive img-portfolio img-hover',
                                    'id' => 'g'
                                ];
                                echo img($product_image);
                                ?>
                                <style>#t {width: 230px;height: 75px;overflow: scroll;}</style>
                                <p id="t"> <?= $come->DESCRICAO ?></p>
                                <p><code>Price:</code> <?= $come->VALOR ?> </p>
                                <?php if ($this->session->userdata('grupo') != '1' and $this->session->userdata('grupo') != '2'): ?>
                                    <?= anchor('home/add_to_cart/' . $come->ANUNCIO_ID . '/' . $send, 'Add To Cart || Buy', ['class' => 'btn btn-success  btn-xs', 'role' => 'button']) ?>
                                    <ul class="nav nav-tabs navbar-right">
                                        <li class="dropdown">
                                            <a class="dropdown-toggle" data-toggle="dropdown">Report <i class="fa fa-exclamation-triangle"></i></a> 
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <?= anchor('home/report/' . $come->ANUNCIO_ID, "I don't  like this Product", ["class'=>'btn  btn-xs"]) ?>
                                                </li>
                                            </ul>
                                        </li> 
                                    </ul>
                                <?php else: ?>
                                    <?= anchor('admin/products/edit/' . $come->ANUNCIO_ID, 'Edit', ['class' => 'btn btn-success btn-xs']) ?>
                                    <?=
                                    anchor('admin/products/delete/' . $come->ANUNCIO_ID, 'Delete', ['class' => 'btn btn-danger btn-xs',
                                        'onclick' => 'return confirm(\'Are You Sure ? \')'
                                    ])
                                    ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div> 
                <?php endforeach; ?>
            </div>
            <!-- /.row -->
            <hr>

            <!-- Footer -->
            <?php $this->load->view('layout/footer') ?>
        </div>

    </body>
</html>
