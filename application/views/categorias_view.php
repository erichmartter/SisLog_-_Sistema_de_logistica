<?php include 'includes/inc_header.php'; ?>
<?php include 'includes/inc_menuLateral.php'; ?>
<?php include 'includes/inc_menuSuperior.php'; ?>

<!DOCTYPE html>
<html lang="en">
    <body class="hold-transition skin-blue sidebar-mini">

        <?php if ($this->session->userdata('grupo') == '1' or $this->session->userdata('grupo') == '2'): ?>
            <?php include 'includes/inc_menuLateral.php'; ?>
        <div class="content-wrapper">
            <?php else:?>
            <?php redirect(''.base_url());?>
        <?php endif; ?>
            <div class="col-xs-8">

            </div>


            <div style="padding: 3px" class="col-sm-2" data-toggle="modal" data-target="#modal-categoria">
                <div class="btn btn-success btn-sm">
                    <span class="glyphicon glyphicon-new-window"></span> Nova Categoria</div>
            </div>
            <table class="table table-responsive">
                <thead>
                    <tr>
                        <th>Código</th>
                        <th>Categoria</th>
                        <th>Setor</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>    
                    <?php foreach ($categorias as $categoria) { ?>
                        <tr>
                            <td> <?= $categoria->CATEGORIA_ID ?> </td>  
                            <td> <?= $categoria->DESC_CATEGORIA ?> </td>
                            <td> <?= $categoria->SETOR ?> </td>
                            <td> 
                                <a href="<?= base_url() . 'categorias/alterar/' . $categoria->CATEGORIA_ID ?>">
                                    Alterar
                                    <span class="glyphicon glyphicon-pencil" title="Alterar"></span>
                                </a> &nbsp;&nbsp;

                                <a href="<?= base_url() . 'categorias/del/' . $categoria->CATEGORIA_ID ?>"
                                   onclick="return confirm('Confirma Exclusão da Categoria \'<?= $categoria->DESC_CATEGORIA ?>\'?')">
                                    Excluir
                                    <span class="glyphicon glyphicon-remove" title="Excluir"></span>
                                </a>
                            </td>
                        </tr>    
                    <?php } ?>
                </tbody>
            </table> 
            
            
            <div class="modal fade" id="modal-altCliente">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Cadastro de Categoria</h4>
                        </div>
                        <div class="modal-body">
                            <form class="form-horizontal" action="clientes/grava_inclusao" method="POST" enctype="multipart/form-data">
                                <fieldset>
                                    <input type="hidden" name="id" value="<?= $cliente->PESSOA_ID ?>">


                                    <div class="col-lg-12 form-group margin50">
                                        <label class="col-lg-2"  for="DESC_PRODUTO">Produto</label>
                                        <div class="col-lg-4">
                                            <input type="text" id="NOME" name="NOME" placeholder="" class="form-control name" required="true">
                                        </div>
                                    </div>

                                </fieldset>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Fechar</button>
                                    <button type="submit" class="btn btn-primary">Salvar</button>
                                    <button type="reset" class="btn btn-default">Limpar</button>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modal-categoria">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Cadastro de Categoria</h4>
                        </div>
                        <div class="modal-body">
                            <form class="form-horizontal" action="categorias/grava_inclusao" method="POST" enctype="multipart/form-data">
                                <fieldset>
                                    <div class="col-lg-12 form-group margin50">
                                        <label class="col-lg-2"  for="DESC_CATEGORIA">Descrição</label>
                                        <div class="col-lg-4">
                                            <input type="text" id="DESC_CATEGORIA" name="DESC_CATEGORIA" placeholder="" class="form-control name" required="true">
                                        </div>
                                    </div>
                                    
                                    <div class="col-lg-12 form-group" >
                                    <label class="col-lg-2" for="SETOR">Setor</label>
                                    </div>

                                </fieldset>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Fechar</button>
                                    <button type="submit" class="btn btn-primary">Salvar</button>
                                    <button type="reset" class="btn btn-default">Limpar</button>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </body>
</html>