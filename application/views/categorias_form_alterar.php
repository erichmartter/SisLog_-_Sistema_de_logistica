<!DOCTYPE html>
<html lang="en">
    
    <?php include 'includes/inc_header.php'; ?>
    <?php include 'includes/inc_menuLateral.php'; ?>
    <?php include 'includes/inc_menuSuperior.php'; ?>
    <body class="hold-transition skin-blue sidebar-mini">

        
        <?php if ($this->session->userdata('grupo') == '1' or $this->session->userdata('grupo') == '2'): ?>
            <?php include 'includes/inc_menuLateral.php'; ?>
        <div class="content-wrapper">
            <?php else:?>
            <?php redirect(''.base_url());?>
        <?php endif; ?>
        
           
            <form  method="post" action="<?= base_url('categorias/grava_alteracao') ?>"
                  enctype="multipart/form-data">

                <input type="hidden" name="CATEGORIA_ID" value="<?= $categoria->CATEGORIA_ID ?>">
                
                <div class="col-sm-8">
                    <div class="form-group">
                        <label for="DESC_CATEGORIA"> <br> Categoria </label>
                        <input type="text" id="DESC_CATEGORIA" name="DESC_CATEGORIA" 
                               value="<?= $categoria->DESC_CATEGORIA ?>"
                               class="form-control" required>
                    </div>
                </div>
                <div class="col-sm-12">
                    <button type="submit" class="btn btn-success">Enviar</button>
                    <button type="reset" class="btn btn-default">Limpar</button>
                </div>    
            </form>
        </div>
    </body>
</html>
