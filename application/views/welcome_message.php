<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>


    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">

            <!--Include cabeçalho-->
            <?php include 'includes/inc_menuSuperior.php'; ?>
            <!--Include do header-->
            <?php include 'includes/inc_menuLateral.php'; ?>
            <!--Include do header-->
            <?php include 'includes/inc_header.php'; ?>


            <!-- corpo da página -->
            <div class="content-wrapper">



                	<!-- /.row -->
			<div class="row">
				<!-- body items -->
	
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>Usuários</h4>
							
						</div>
						<div class="panel-body">
						<?php foreach ($members as $member ) : ?>
						<div class="col-md-12">
						<hr>
							<div class="col-md-1">
								
							</div>
							<div class="col-md-3">
								<h4>ID do Usuário</h4>
									<?=  $member->USUARIO_ID  ?>
							</div>
							
							<div class="col-md-3">
								<h4>Nome </h4>
								<?=  $member->USUARIO_NOME  ?>
							</div>
							
							<div class="col-md-3">
								<h4>Grupo</h4>
								<?php if ($member->USUARIO_GRUPO == '1' ):?>
								<?php echo "administrator"  ;?>
								<?php endif;?>
								<?php if ($member->USUARIO_GRUPO == '2' ):?>
								<?php  echo "C-administrator"  ;?>
								<?php endif;?>
								<?php if ($member->USUARIO_GRUPO == '3' ):?>
								<?php  echo "Members"  ;?>
								<?php endif;?>
								
							</div>

							<div class="col-md-2">
                                                            	<?php  if($this->session->userdata('grupo')	==	'1' ): ?>
								<h4>Status do Usuário</h4>
								<?php if ($member->USUARIO_ID == '1' ):?>
								<?php echo "administrator"?>
								<?php else:?>
								<?php if ($member->STATUS == '1' and $member->USUARIO_ID != '1' ):?>
								
								
								<?=  anchor('membros/disable_usr/'.$member->USUARIO_ID,'Desativado ',['class'=>'btn btn-danger btn-xs ',
									'onclick'=>'return confirm(\'Tem certeza que deseja desabilitar este usuário? \')'
								])  ?>
								
								<?=  anchor('#','Ativar',['class'=>'btn btn-success btn-xs disabled '
								])  ?>
								<?php else:?>
								<?=  anchor('#','Desativar ',['class'=>'btn btn-danger btn-xs disabled'])  ?>
								
								<?=  anchor('Membros/active_usr/'.$member->USUARIO_ID,'Ativo ',['class'=>'btn btn-success btn-xs ',
									'onclick'=>'return confirm(\'Tem certeza que deseja ativar este usuário? \')'
								])  ?>
								<?php endif;?>
								<?php endif;?>
								<?php endif;?>
								
							</div>
						
						</div>
						<?php endforeach; ?>	
						</div>
					</div>
				</div> 
				
			</div>
			<!-- /.row -->
			
			<!-- Features Section -->
			
			<!-- /.row -->
			<hr>
			
			<!-- Footer -->
			<?php $this->load->view('layout/footer')?>

            </div>

        </div>



        <div class="modal fade" id="modal-produto">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Cadastro de Produto</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal" action="produtos/grava_inclusao" method="POST" enctype="multipart/form-data">
                            <fieldset>
                                <div class="col-lg-12 form-group margin50">
                                    <label class="col-lg-2"  for="DESC_PRODUTO">Produto</label>
                                    <div class="col-lg-4">
                                        <input type="text" id="nome" name="DESC_PRODUTO" placeholder="" class="form-control name" required="true">
                                    </div>
                                </div>

                                <div class=" col-lg-12 form-group">
                                    <label class="col-lg-2" for="CATEGORIA">Categoria</label>
                                    <div class="col-lg-4">
                                        <input type="text" id="CATEGORIA" name="CATEGORIA" placeholder="" class="form-control name" required="true">
                                    </div>

                                </div>
                                <div class=" col-lg-12 form-group">
                                    <label class="col-lg-2" for="SETOR">Setor</label>
                                    <div class="col-lg-4">
                                        <input type="text" id="SETOR" name="SETOR" placeholder="" class="form-control name" required="true">
                                    </div>

                                </div>

                            </fieldset>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Fechar</button>
                                <button type="submit" class="btn btn-primary">Salvar</button>
                                <button type="reset" class="btn btn-default">Limpar</button>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-cliente">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Cadastro de Cliente</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal" action="clientes/grava_inclusao" method="POST" enctype="multipart/form-data">
                            <fieldset>
                                <div class="col-sm-12 form-group margin50">
                                    <label class="col-lg-2"  for="NOME">Cliente</label>
                                    <div class="col-lg-4">
                                        <input type="text" id="NOME" name="NOME" placeholder="" class="form-control name" required="true">
                                    </div>
                                </div>
                                <div class="col-lg-12 form-group margin50">
                                    <label class="col-lg-2"  for="CPFCNPJ">CNPJ/CPF</label>
                                    <div class="col-lg-4">
                                        <input type="text" id="CPFCNPJ" name="CPFCNPJ" placeholder="" class="form-control name" required="true">
                                    </div>
                                </div>
                                <div class="col-lg-12 form-group margin50">
                                    <label class="col-lg-2"  for="TELEFONE">Telefone</label>
                                    <div class="col-lg-4">
                                        <input type="text" id="TELEFONE" name="TELEFONE" placeholder="" class="form-control name" required="true">
                                    </div>
                                </div>
                                <div class="col-lg-12 form-group margin50">
                                    <label class="col-lg-2"  for="CIDADE_ID">Cidade</label>
                                    <div class="col-lg-4">
                                        <input type="text" id="CIDADE_ID" name="CIDADE_ID" placeholder="" class="form-control name" required="true">
                                    </div>
                                </div>
                                <div class="col-lg-12 form-group margin50">
                                    <label class="col-lg-2"  for="ENDERECO">Endereço</label>
                                    <div class="col-lg-4">
                                        <input type="text" id="ENDERECO" name="ENDERECO" placeholder="" class="form-control name" required="true">
                                    </div>
                                </div>
                                <div class="col-lg-12 form-group margin50">
                                    <label class="col-lg-2"  for="EMAIL">Email</label>
                                    <div class="col-lg-4">
                                        <input type="text" id="EMAIL" name="EMAIL" placeholder="" class="form-control name" required="true">
                                    </div>
                                </div>

                            </fieldset>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Fechar</button>
                                <button type="submit" class="btn btn-primary">Salvar</button>
                                <button type="reset" class="btn btn-default">Limpar</button>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>




    </div>
</body>
</html>


