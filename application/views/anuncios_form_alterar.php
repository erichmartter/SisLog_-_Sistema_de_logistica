<?php 
	$ANUNCIO_ID 				= $anuncio->ANUNCIO_ID;
if($this->input->post('is_submitted'))
{
		$NOME 			= set_value('NOME');
		$DESCRICAO	= set_value('DESCRICAO');
		$TIPO_ANUNCIO			= set_value('TIPO_ANUNCIO');
		$VALOR			= set_value('VALOR');
		$ALTURA			= set_value('ALTURA');
}else{

		$NOME 			= $anuncio->NOME;
		$TIPO_ANUNCIO			= $anuncio->TIPO_ANUNCIO;
		$DESCRICAO	= $anuncio->DESCRICAO;
		$VALOR			= $anuncio->VALOR;
		$ALTURA			= $anuncio->ALTURA;
	
}//end if 	is_submitted
	
?>
<!DOCTYPE html>

<?php if ($this->session->userdata('grupo') == '3'||'1'): ?>
        <?php include 'includes/inc_menuSuperior_front.php'; ?>
    <?php endif; ?>
        <?php include 'includes/inc_header_front.php'; ?>

<html lang="en">


	
	<body>
		
		<!-- Header- dash_menu -->
		<!-- Page Content -->
		<div class="container-fluid">
			<!-- /.row -->
			<div class="row">
				<!-- body items -->
	
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>
								<i class="fa fa-fw fa-compass"></i>  Products Edit
							</h4>
						</div><!-- /..panel-heading -->
						<div class="panel-body">
						<div><?= validation_errors()?></div>
						<?=  form_open_multipart('anuncios/edit/'.$ANUNCIO_ID,['class'=>'form-group']) ?>
							<div class="col-sm-4">
								<div class="input-group">
									<div class="input-group-addon">Name</div>
									<input type="text" class="form-control" name="NOME" placeholder="Enter Product Primary Name" value="<?= $NOME ?>">
								</div>
							</div>
							
							<div class="col-sm-4">
								<div class="input-group">
									<div class="input-group-addon">Title</div>
									<input type="text" class="form-control" name="TIPO_ANUNCIO" placeholder="Enter Product Title" value="<?= $TIPO_ANUNCIO ?>">
								</div>
							</div>
						
							<div class="input-group-addon">Description</div>
							<div class="col-sm-4">
								<div class="input-group col-sm-12">
									<textarea rows="4" class="form-control" name="DESCRICAO" placeholder="Enter Product Description , Example : Dell INSPIRON Ram:2GB , AVG : 1 , CPU : 3200 Intel Core i5"><?= $DESCRICAO ?></textarea>
								</div>
							</div>
							<div class="col-sm-12"><hr></div>
							<div class="col-sm-3">
								<div class="input-group">
									<div class="input-group-addon">Price</div>
									<input type="text" class="form-control" name="VALOR" placeholder="Enter Product Price" value="<?= $VALOR ?>">
									<div class="input-group-addon">$</div>
								</div>
							</div>
							
							<div class="col-sm-3">
								<div class="input-group">
									<div class="input-group-addon">Available Stock</div>
									<input type="text" class="form-control" name="ALTURA" value="<?= $ALTURA ?>">
								</div>
							</div>
							
							<div class="col-sm-3">
								<div class="input-group">
									<input type="file" name="userfile">
								</div>
							</div>
							
							<div class="col-sm-1">
								<div class="input-group">
									<input type="hidden" name="is_submitted" value="1">
									<button type="submit" class="btn btn-success">Update</button>
								</div>
							</div>
							<div class="col-sm-1">
								<div class="input-group">
									
									<?=  anchor('home','Cancel',['class'=>'btn btn-danger']) ?>
								</div>
							</div>
							
						
						<?= form_close() ?>
						</div><!-- /..panel-body -->
					</div><!-- /..panel panel-default -->
				</div> 
				
			</div>
			<!-- /.row -->
			<hr>
			<!-- Footer -->
			<?php $this->load->view('layout/footer')?>
			
		</div>
		<!-- /.. container -->
		
		<!-- jQuery -->
		<script src="js/jquery.js"></script>
		
		<!-- Bootstrap Core JavaScript -->
		<script src="js/bootstrap.min.js"></script>
	</body>
</html>
