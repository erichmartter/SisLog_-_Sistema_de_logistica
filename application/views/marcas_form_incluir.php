<!DOCTYPE html>
<html lang="en">
    
    <?php include 'includes/inc_head.php'; ?>
    <?php include 'includes/inc_menu_lateral.php'; ?>
    <?php include 'includes/inc_menu_superior.php'; ?>
    <body>
        <div class="container">
           
            <form method="post" action="<?= base_url('marcas/grava_inclusao') ?>"
                  enctype="multipart/form-data">

                <div class="col-sm-8">
                    <div class="form-group">
                        <label for="modelo"><br> Marca </label>
                        <input type="text" id="nome" name="nome" 
                               class="form-control" required>
                    </div>
                </div>

               
                
                <div class="col-sm-12">
                    <button type="submit" class="btn btn-success">Enviar</button>
                    <button type="reset" class="btn btn-default">Limpar</button>
                </div>    
            </form>
        </div>
    </body>
</html>
