<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Clientes extends CI_Controller {

    public function __construct() {
        parent::__construct();

        // carrega a model a ser utilizada neste controller
        $this->load->model('clientes_model', 'clientesM');
    }

    public function index() {
        // obtém os dados do model clientes_model
        $dados['clientes'] = $this->clientesM->select();

        $this->load->view('clientes_view', $dados);
    }

    public function incluir() {
        // obtém as clientes
        $dados['clientes'] = $this->clientesM->select();

        $this->load->view('clientes_form_incluir', $dados);
    }

    public function grava_inclusao() {
        // recebe os dados do formulário
        $dados = $this->input->post();


        if ($this->clientesM->insert($dados)) {
            $mensa = "Cliente cadastrado";
            $tipo = 1;
        } else {
            $mensa = "Cliente Não Cadastrado";
            $tipo = 0;
        }

        // atribui para variáveis de sessão "flash"
        $this->session->set_flashdata('mensa', $mensa);
        $this->session->set_flashdata('tipo', $tipo);

        // recarrega a view (index)
        redirect(base_url('clientes'));
    }

    public function alterar($id) {
        // obtém os campos do veículo cujo id foi passado por parâmetro
        $dados['cliente'] = $this->clientesM->find($id);

        $this->load->view('clientes_form_alterar', $dados);
    }

    public function grava_alteracao() {
        // recebe os dados do formulário
        $dados = $this->input->post();
        $this->clientesM->update($dados);
        // recarrega a view (index)
        redirect(base_url('clientes'));
    }
    
    function del($id){
           // cláusula where do delete
        $this->db->where('PESSOA_ID', $id);
        // altera os dados
         $this->db->delete('pessoa'); 
         redirect(base_url('clientes'));
        
    }

}
