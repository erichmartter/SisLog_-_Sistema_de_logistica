<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Anuncios extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('grupo') != ('1' || '2' || '3')) {
            $this->session->set_flashdata('error', 'Você precisa estar logado');
            redirect('login');
        }

        $this->load->model('Model_anuncios');
        $this->load->model('Model_usuarios');
        $this->load->model('Tipos_model');
        $this->load->model('Model_tiposAnuncios');
        $this->load->model('Model_configuracoes');
    }

    public function index() {
        $data['anuncios'] = $this->Model_anuncios->all_anuncios();
        $data['tiposAnuncios'] = $this->Model_tiposAnuncios->all_tipoanuncio();
        $data['get_sitename'] = $this->Model_configuracoes->nome_site_config();
        $data['get_footer'] = $this->Model_configuracoes->footer_config();
        $data['tipos'] = $this->Tipos_model->select();

        $this->load->view('teste', $data);
    }

    public function single($pro_id) {

        $data['anuncios'] = $this->Model_anuncios->find($pro_id);
        $data['get_sitename'] = $this->Model_configuracoes->nome_site_config();
        $data['get_footer'] = $this->Model_configuracoes->footer_config();
        $this->load->view('anuncio_view', $data);
    }
    public function criar() {
        $data['anuncios'] = $this->Model_anuncios->all_anuncios();
        $data['tiposAnuncios'] = $this->Model_tiposAnuncios->all_tipoanuncio();
        $data['get_sitename'] = $this->Model_configuracoes->nome_site_config();
        $data['get_footer'] = $this->Model_configuracoes->footer_config();
        $data['tipos'] = $this->Tipos_model->select();
        $this->load->view('teste',$data);
    }

    public function create() {
        $data['get_sitename'] = $this->Model_configuracoes->nome_site_config();
        $data['get_footer'] = $this->Model_configuracoes->footer_config();
        $data['tipos'] = $this->Tipos_model->select();
        $this->load->model('Model_tiposAnuncios');


        $this->form_validation->set_rules('TIPO_ANUNCIO', 'Tipo Anúncio', 'required');
        $this->form_validation->set_rules('NOME', 'Product Name', 'required');
        $this->form_validation->set_rules('DESCRICAO', 'Product Description', 'required');
        $this->form_validation->set_rules('VALOR', 'Product Price', 'required|integer');
        $this->form_validation->set_rules('ALTURA', 'Product Price', 'required|integer');
        $this->form_validation->set_rules('LARGURA', 'Product Price', 'required|integer');
        $this->form_validation->set_rules('PROFUNDIDADE', 'Product Price', 'required|integer');
        $this->form_validation->set_rules('PESO', 'Product Price', 'required|integer');
        $this->form_validation->set_rules('PRAZO_ENTREGA', 'Prazo', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('anuncios_form_incluir', $data);
        } else {
            //load uploading file 
            $config['upload_path'] = './assets/uploads/';
            $config['allowed_types'] = 'jpg|png';
            $config['max_size'] = 2048000; // = MB
            $config['max_width'] = 2000;
            $config['max_height'] = 2000;
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload()) {
                $this->load->view('anuncios_form_incluir');
            } else {
                // if form_validation = true  -> insert into db
                $upload_image = $this->upload->data();
                $data_products = array
                    (
                    'NOME' => set_value('NOME'),
                    'TIPO_ANUNCIO' => set_value('TIPO_ANUNCIO'),
                    'DESCRICAO' => set_value('DESCRICAO'),
                    'VALOR' => set_value('VALOR'),
                    'PRAZO_ENTREGA' => set_value('PRAZO_ENTREGA'),
                    'ANUNCIO_IMAGEM' => $upload_image['file_name']
                ); //end array data_products

                $this->Model_anuncios->create($data_products);
                redirect('home');
            } //end if uploading 
        }//end if form_validation
    }

///end class create ///

    public function edit($ANUNCIO_ID) {
        $data['get_sitename'] = $this->Model_configuracoes->nome_site_config();
        $data['get_footer'] = $this->Model_configuracoes->footer_config();
        $this->form_validation->set_rules('TIPO_ANUNCIO', 'Tipo', 'required');
        $this->form_validation->set_rules('NOME', 'Nome', 'required');
        $this->form_validation->set_rules('DESCRICAO', 'Descrição', 'required');
        $this->form_validation->set_rules('VALOR', 'Valor', 'required|integer');
        $this->form_validation->set_rules('ALTURA', 'Altura', 'required|integer');
        $this->form_validation->set_rules('LARGURA', 'Largura', 'required|integer');
        $this->form_validation->set_rules('PROFUNDIDADE', 'Profundidade', 'required|integer');
        $this->form_validation->set_rules('PESO', 'Peso', 'required|integer');
        $this->form_validation->set_rules('PRAZO_ENTREGA', 'Prazo de entrega', 'required');

        if ($this->form_validation->run() == FALSE) {
            $data['anuncio'] = $this->Model_anuncios->find($ANUNCIO_ID);
            $data['tipo'] = $this->Tipos_model->select($ANUNCIO_ID);
            $this->load->view('anuncios_form_alterar', $data);
        } else {
            if ($_FILES['userfile']['name'] != '') {
                //load uploading file 
                $config['upload_path'] = './assets/uploads/';
                $config['allowed_types'] = 'jpg|png';
                $config['max_size'] = 2000; // = MB
                $config['max_width'] = 2000;
                $config['max_height'] = 2000;
                $this->load->library('upload', $config);

                if (!$this->upload->do_upload()) {
                    $data['product'] = $this->Model_anuncios->find($ANUNCIO_ID);
                    $this->load->view('anuncios_form_alterar', $data);
                } else {
                    $upload_image = $this->upload->data();
                    $data_products = array(
                        'pro_name' => set_value('pro_name'),
                        'pro_title' => set_value('pro_title'),
                        'DESCRICAO' => set_value('DESCRICAO'),
                        'VALOR' => set_value('VALOR'),
                        'pro_stock' => set_value('pro_stock'),
                        'pro_image' => $upload_image['file_name']
                    ); //end array data_products
                    $this->Model_anuncios->edit($ANUNCIO_ID, $data_products);
                    redirect('admin/products');
                }//end if uploading
            } else {
                $data_products = array(
                    'pro_name' => set_value('pro_name'),
                    'pro_title' => set_value('pro_title'),
                    'DESCRICAO' => set_value('DESCRICAO'),
                    'VALOR' => set_value('VALOR'),
                    'pro_stock' => set_value('pro_stock'),
                ); //end array data_products
                $this->Model_anuncios->edit($ANUNCIO_ID, $data_products);
                redirect('admin/products');
            }//end if FILES
        }//end if form_validation
    }

//end function update

    public function delete($ANUNCIO_ID) {
        $this->Model_anuncios->delete($ANUNCIO_ID);
        redirect('admin/products');
    }

}
