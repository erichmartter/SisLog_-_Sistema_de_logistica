<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Membros extends CI_Controller {
		
	public function __construct ()
	{
		parent::__construct();
		if($this->session->userdata('grupo')!=	('1' ||'2') )
		{
			$this->session->set_flashdata('error','Desculpe, você precisa estar logado');
			redirect('login');	
		}
		
		//load model -> Model_produtos
		$this->load->model('Model_produtos');
		$this->load->model('Model_usuarios');
		$this->load->model('Model_configuracoes');
	}
	
	public function index()
	{	
		$data['get_sitename'] = $this->Model_configuracoes->nome_site_config();
		$data['get_footer'] = $this->Model_configuracoes->footer_config();
                		$data['members'] = $this->Model_usuarios->members();	

		$this->load->view('welcome_message',$data);
	}
	
	public function reports()
	{
		$data['get_sitename'] = $this->Model_configuracoes->nome_site_config();
		$data['get_footer'] = $this->Model_configuracoes->footer_config();	
		$data['reports'] = $this->Model_produtos->reports();	
		$this->load->view('backend/reports',$data);
	}
	
	public function del_report($rep_id_product)
	{
		$this->Model_produtos->del_report($rep_id_product);
		redirect('admin/products/reports');	
	}
	
	public function members()
	{
		$data['get_sitename'] = $this->Model_configuracoes->nome_site_config();
		$data['get_footer'] = $this->Model_configuracoes->footer_config();	
		$data['members'] = $this->Model_usuarios->members();	
		$this->load->view('welcome_message',$data);
	}
	
	public function active_usr($usr_id)
	{
		$active = '1';
		$data_user = array
		(
			'STATUS'			=> $active
		);
		$this->Model_usuarios->active($usr_id,$data_user);
		redirect('Membros/members');
	}
	
	public function disable_usr($usr_id)
	{
		$active = '0';
		$data_user = array
		(
		'STATUS'			=> $active
		);
		$this->Model_usuarios->active($usr_id,$data_user);
		redirect('Membros/members');
	}
	

	

}
