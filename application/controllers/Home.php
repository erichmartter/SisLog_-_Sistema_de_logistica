<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Produtos_model');
        $this->load->model('Model_anuncios');
        $this->load->model('Tipos_model');
        $this->load->model('Model_produtos');
        $this->load->model('Model_usuarios');
        $this->load->model('Model_configuracoes');
        date_default_timezone_set('America/Sao_Paulo');
    }

    function index() {
        $data['get_sitename'] = $this->Model_configuracoes->nome_site_config();
        $data['get_footer'] = $this->Model_configuracoes->footer_config();
        $data['produtos'] = $this->Model_produtos->all_products(); //this all_products from model 
        $data['anuncios'] = $this->Model_anuncios->all_anuncios(); //this all_products from model 
        $data['tipos'] = $this->Tipos_model->select(); //this all_products from model 
        $data['starts'] = $this->Model_produtos->dis_products();
        $data['starts'] = $this->Model_anuncios->dis_anuncios();
        $this->load->view('front/home', $data); //this $data from model inside class Model_produtos
    }

    public function showme($pro_name) {
        $data['get_sitename'] = $this->Model_configuracoes->nome_site_config();
        $data['get_footer'] = $this->Model_configuracoes->footer_config();
        $data['starts'] = $this->Model_anuncios->dis_anuncios();
        $data['tipos'] = $this->Tipos_model->select();
        $data['comes'] = $this->Model_anuncios->showme($pro_name); //for showme function in home/showme
        $data['anuncios'] = $this->Model_anuncios->showme($pro_name); //for showme function in home/showme
        $this->load->view('front/home', $data);
    }
    
    public function add_to_cart($pro_id)
	{
		$product = $this->Model_produtos->find($pro_id);
		$data = array(
						'id'      => $product->PRODUTO_ID,
						'qty'     => 1,
						'price'   => $product->VALOR,
						'name'    => $product->CATEGORIA,
						'title'	  => $product->PRODUTO_NOME
						);
		
		$this->cart->insert($data);	
		if ($pro_id == 'add')
		{
				redirect('home/showme/'.$product->CATEGORIA);
		}else{
				redirect(base_url());
			 }
		
	}
        
        public function cart()
	{
		$data['get_sitename'] = $this->Model_configuracoes->nome_site_config();
		$data['get_footer'] = $this->Model_configuracoes->footer_config();	
		$this->load->view('show_cart',$data);	
		
	}
        
        public function clear_cart()
	{
		$this->cart->destroy();
		redirect(base_url());
	}
	
	public function report($pro_id)
	{
		$product = $this->Model_produtos->find($pro_id);
		
		if($this->session->userdata('group')	!=	('2' ||'3'))
		{
			$group_usr = Gost;
			$name_usr = Gost;	
		}else{
				$group_usr = $this->session->userdata('group');
				$name_usr = $this->session->userdata('username');
			}
		
		
		
		$report_products = array
		(
			'rep_id_product'			=> $product->pro_id,
			'rep_name'					=> $product->pro_name,
			'rep_title_product'			=> $product->pro_title,
			'rep_usr_name'				=> $name_usr,
			'rep_usr_group'				=> $group_usr
		);
		$this->Model_produtos->report($report_products);
		redirect(base_url());
		
	}

}
