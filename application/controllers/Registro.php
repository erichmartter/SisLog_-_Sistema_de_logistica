<?php

class Registro extends CI_Controller {
    
    function __construct() {
        
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Model_usuarios');
        $this->load->model('Model_configuracoes');
        date_default_timezone_set('America/Sao_Paulo');
    }
    
  public function index()
	{
		
		$this->form_validation->set_rules('rnome','Username','required|alpha_numeric|min_length[4]|max_length[20]');
		$this->form_validation->set_rules('rsenha','Password','required|alpha_numeric|matches[resenha]|min_length[6]|max_length[32]');
		$this->form_validation->set_rules('resenha','Password','required|alpha_numeric');
		
		if($this->form_validation->run()	==	FALSE)
		{
			$data['get_sitename'] = $this->Model_configuracoes->nome_site_config();
			$data['get_footer'] = $this->Model_configuracoes->footer_config();	
			$this->load->view('registro_view',$data);
                        
                }else{
				$data_register_new = array
				 (
					'USUARIO_NOME'			=> set_value('rnome'),
					'USUARIO_SENHA'		=> set_value('rsenha'),
					'STATUS'				=> '1',
					'USUARIO_GRUPO'				=>'3'
				 );
				 if($this->Model_usuarios->is_usr() == FALSE)
				 {
					 $this->Model_usuarios->register_new($data_register_new);
						 $this->form_validation->set_rules('rnome');
						 $this->form_validation->set_rules('rsenha');
						 if($this->form_validation->run()	==	FALSE)
						 {
								$this->load->view('login_view'); 	
						 }else{
								$valid_user	= $this->Model_usuarios->check();
								 if($valid_user	==	FALSE)
								 {
									 $this->session->set_flashdata('error','Usuário ou senha incorretos!' );
									 redirect('login');
								 }else{
										 $this->session->set_userdata('nome',$valid_user->USUARIO_NOME);
										 $this->session->set_userdata('grupo',$valid_user->USUARIO_GRUPO);
										 switch($valid_user->USUARIO_GRUPO)
										 {
											 case 3 ://Membro
											 redirect(base_url());
											 break;
											 
											 default: break;
										 }
								 }
						 }
					 
					 redirect(base_url());
				 }else{
						$this->session->set_flashdata('error','Por favor, insira um nome de usuário diferente' );
						redirect('Registro');
				 }
		}
	}
	

}
