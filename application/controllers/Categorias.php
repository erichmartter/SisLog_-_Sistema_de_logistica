<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Categorias extends CI_Controller {

    public function __construct() {
        parent::__construct();

        // carrega a model a ser utilizada neste controller
        $this->load->model('categorias_model', 'categoriasM');
    }

    public function index() {
        // obtém os dados do model categorias_model
        $dados['categorias'] = $this->categoriasM->select();

        $this->load->view('categorias_view', $dados);
    }

    public function incluir() {
        // obtém as categorias
        $dados['categorias'] = $this->categoriasM->select();

        $this->load->view('categorias_form_incluir', $dados);
    }

    public function grava_inclusao() {
        // recebe os dados do formulário
        $dados = $this->input->post();


        if ($this->categoriasM->insert($dados)) {
            $mensa = "Categoria cadastrada";
            $tipo = 1;
        } else {
            $mensa = "Categoria Não Cadastrada";
            $tipo = 0;
        }

        // atribui para variáveis de sessão "flash"
        $this->session->set_flashdata('mensa', $mensa);
        $this->session->set_flashdata('tipo', $tipo);

        // recarrega a view (index)
        redirect(base_url('categorias'));
    }

    public function alterar($id) {
        // obtém os campos do veículo cujo id foi passado por parâmetro
        $dados['categoria'] = $this->categoriasM->find($id);

        $this->load->view('categorias_form_alterar', $dados);
    }

    public function grava_alteracao() {
        // recebe os dados do formulário
        $dados = $this->input->post();
        $this->categoriasM->update($dados);
        // recarrega a view (index)
        redirect(base_url('categorias'));
    }

    function excluir($id) {
        $query = "DELETE * FROM categorias WHERE CATEGORIA_ID = '$id';";
        # The cat's leap: teste as tabelas antes de tentar excluir o principal
        $this->db->where('CATEGORIA_ID', $id);
        $test = $this->db->get('categorias');
        if (empty($test->result_array())) {
            echo 'execute $query';
            $this->db->query($query);
            $mensa .= "Registro corretamente excluído"; 
            $tipo = 1;
        } else {
            echo 'show some error';
            $mensa .= "Não foi possível excluir o registro";
            $tipo = 0;
        }
        // atribui para variáveis de sessão "flash"
        $this->session->set_flashdata('mensa', $mensa);
        $this->session->set_flashdata('tipo', $tipo);

        // recarrega a view (index)
        redirect(base_url('categorias')); 
    }
    
    function del($id){
           // cláusula where do delete
        $this->db->where('CATEGORIA_ID', $id);
        // altera os dados
         $this->db->delete('categorias'); 
         redirect(base_url('categorias'));
        
    }

}
