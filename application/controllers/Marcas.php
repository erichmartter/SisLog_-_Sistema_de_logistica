<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Marcas extends CI_Controller {

    public function __construct() {
        parent::__construct();

        // carrega a model a ser utilizada neste controller
        $this->load->model('marcas_model', 'marcasM');
    }

    public function index() {
        // obtém os dados do model marcas_model
        $dados['marcas'] = $this->marcasM->select();

        $this->load->view('marcas_view', $dados);
    }

    public function incluir() {
        // obtém as marcas
        $dados['marcas'] = $this->marcasM->select();

        $this->load->view('marcas_form_incluir', $dados);
    }

    public function grava_inclusao() {
        // recebe os dados do formulário
        $dados = $this->input->post();


        if ($this->marcasM->insert($dados)) {
            $mensa = "Marca cadastrada";
            $tipo = 1;
        } else {
            $mensa = "Marca Não Cadastrada";
            $tipo = 0;
        }

        // atribui para variáveis de sessão "flash"
        $this->session->set_flashdata('mensa', $mensa);
        $this->session->set_flashdata('tipo', $tipo);

        // recarrega a view (index)
        redirect(base_url('marcas'));
    }

    public function alterar($id) {
        // obtém os campos do veículo cujo id foi passado por parâmetro
        $dados['marca'] = $this->marcasM->find($id);

        $this->load->view('marcas_form_alterar', $dados);
    }

    public function grava_alteracao() {
        // recebe os dados do formulário
        $dados = $this->input->post();
        $this->marcasM->update($dados);
        // recarrega a view (index)
        redirect(base_url('marcas'));
    }

    function excluir($id) {
        $query = "DELETE * FROM 'marcas' WHERE ID_MARCA = '$id';";
        # The cat's leap: teste as tabelas antes de tentar excluir o principal
        $this->db->where('ID_MARCA', $id);
        $test = $this->db->get('veiculos');
        if (empty($test->result_array())) {
            //$this->db->query($query);
            $this->marcasM->delete($id);
        } else {
            echo 'show some error';
        }
        // atribui para variáveis de sessão "flash"
        $this->session->set_flashdata('mensa', $mensa);
        $this->session->set_flashdata('tipo', $tipo);

        // recarrega a view (index)
        redirect(base_url('marcas'));
    }
    
    function del($id){
           // cláusula where do delete
        $this->db->where('ID_MARCA', $id);
        // altera os dados
         $this->db->delete('marcas'); 
         redirect(base_url('marcas'));
        
    }

}
