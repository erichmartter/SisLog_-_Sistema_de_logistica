<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Produtos extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('grupo') != ('1' || '2')) {
            $this->session->set_flashdata('error', 'Você precisa estar logado');
            redirect('login');
        }

        // carrega a model a ser utilizada neste controller
        $this->load->model('Produtos_model', 'produtosM');
        $this->load->model('Model_usuarios');
        $this->load->model('Model_configuracoes');
    }

    public function index() {
        // obtém os dados do model produtos_model
        $dados['produtos'] = $this->produtosM->select();

        $this->load->view('produtos_view', $dados);
    }

    public function incluir() {
        // obtém as produtos
        $dados['produtos'] = $this->produtosM->select();

        $this->load->view('produtos_form_incluir', $dados);
    }

    public function grava_inclusao() {
        // recebe os dados do formulário
        $dados = $this->input->post();


        if ($this->produtosM->insert($dados)) {
            $mensa = "Produto cadastrada";
            $tipo = 1;
        } else {
            $mensa = "Produto Não Cadastrada";
            $tipo = 0;
        }

        // atribui para variáveis de sessão "flash"
        $this->session->set_flashdata('mensa', $mensa);
        $this->session->set_flashdata('tipo', $tipo);

        // recarrega a view (index)
        redirect(base_url('produtos'));
    }

    public function alterar($id) {
        // obtém os campos do veículo cujo id foi passado por parâmetro
        $dados['produto'] = $this->produtosM->find($id);

        $this->load->view('produtos_form_alterar', $dados);
    }

    public function grava_alteracao() {
        // recebe os dados do formulário
        $dados = $this->input->post();
        $this->produtosM->update($dados);
        // recarrega a view (index)
        redirect(base_url('produtos'));
    }

    function excluir($id) {
        $query = "DELETE * FROM produtos WHERE id = $id;";
        # The cat's leap: teste as tabelas antes de tentar excluir o principal
        $this->db->where('id', $id);
        $this->db->query($query);

        $test = $this->db->get('categorias');
        if (empty($test->result_array())) {
            echo 'execute $query';
            $this->db->query($query);
        } else {
            echo 'show some error';
        }
        redirect(base_url('produtos'));
    }

    function del($id) {
        // cláusula where do delete
        $this->db->where('id', $id);
        // altera os dados
        $this->db->delete('produtos');
        redirect(base_url('produtos'));
    }

}
