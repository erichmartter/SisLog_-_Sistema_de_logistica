<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
		
	public function index()
	{
                $this->load->library('session');
		$this->load->model('Model_configuracoes');
		$this->form_validation->set_rules('nome','Nome','required|alpha_numeric');
		$this->form_validation->set_rules('senha','Senha','required|alpha_numeric');
		
		if($this->form_validation->run()	==	FALSE)
		{
			$data['get_sitename'] = $this->Model_configuracoes->nome_site_config();
			$data['get_footer'] = $this->Model_configuracoes->footer_config();	
			$this->load->view('login_view',$data); 	
                        
		}else{
			$this->load->model('Model_usuarios');	
			$valid_user	= $this->Model_usuarios->check_usr();
			$check_user_is_active = $this->Model_usuarios->check_user_is_active();
			if($valid_user	==	FALSE)
			{
				if ($check_user_is_active == FALSE)
				{
						$this->session->set_flashdata('error','Username / Password Not Correct !' );
				}else{
						$this->session->set_flashdata('error','Sorry this account is not active !' );
					 }
				
				redirect('login');
			}else{
				$this->session->set_userdata('nome',$valid_user->USUARIO_NOME);
				$this->session->set_userdata('grupo',$valid_user->USUARIO_GRUPO);
				
				switch($valid_user->USUARIO_GRUPO)
				{
					case 1 ://for admin
							redirect('Membros/members');
					break;
					
					case 2 ://for c-admin
							redirect('produtos');
					break;
					
					case 3 ://for member
							redirect(base_url());
					break;
					
					default: break;
				}
			}//end if valid_user 
			
		}//end if validation
		
		
		
		
	}
	
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('home');
	}
	
		
	
	
}
