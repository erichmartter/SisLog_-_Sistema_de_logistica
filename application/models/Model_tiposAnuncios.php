<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_tiposAnuncios extends CI_Model {

    public function all_tipoanuncio() {
        $show = $this->db->get('tipoanuncio');
        if ($show->num_rows() > 0) {
            return $show->result();
        } else {
            return array();
        } //end if num_rows
    }

    public function dis_tipoanuncio() {
        $this->db->distinct();
        $query = $this->db->query('SELECT DISTINCT TIPO_ANUNCIO FROM tipoanuncio');
        return $query->result();
    }

    public function showme($pro_name) {

        $query = $this->db->get_where('tipoanuncio', array('TIPO_ANUNCIO' => $pro_name));
        return $query->result();
    }

    public function find($pro_id) {//للبحث عن رقم المنتج وتحقق من وجوده 
        //this is for find record id->product
        $code = $this->db->where('TIPO_ID', $pro_id)
                ->limit(1)
                ->get('tipoanuncio');
        if ($code->num_rows() > 0) {
            return $code->row();
        } else {
            return array();
        }//end if code->num_rows > 0 
    }

    public function create($data_tipoanuncio) {//لانشاء منتج جديد
        //guery insert into database 	
        $this->db->insert('tipoanuncio', $data_tipoanuncio);
    }

    public function edit($pro_id, $data_tipoanuncio) {//للتعديل على المنتج
        //guery update FROM .. WHERE id->tipoanuncio
        $this->db->where('ANUNCIO_ID', $pro_id)
                ->update('tipoanuncio', $data_tipoanuncio);
    }

    public function delete($pro_id) {
        //guery delete id->tipoanuncio
        $this->db->where('ANUNCIO_ID', $pro_id)
                ->delete('tipoanuncio');
    }

    public function report($report_tipoanuncio) {

        $this->db->insert('relato', $report_tipoanuncio);
    }

//end function craete

    public function reports() {
        $report = $this->db->get('relato');
        if ($report->num_rows() > 0) {
            return $report->result();
        } else {
            return array();
        } //end if num_rows
    }

//end function report

    public function del_report($rep_id_product) {
        $this->db->where('RELATO_ID_PRODUTO', $rep_id_product)
                ->delete('relato');
    }

}

//end class Model_tipoanuncio
///////////////////////////////  Model_tipoanuncio : this is use in controller admin/tipoanuncio + home 