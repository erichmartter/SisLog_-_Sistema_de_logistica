<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_anuncios extends CI_Model {

    public function all_anuncios() {
      
        $query2 = $this->db->select('*')
                ->select('tipoanuncio.TIPO_DESC as tipo')
                ->from('anuncios')
                ->join('tipoanuncio', 'anuncios.TIPO_ANUNCIO = tipoanuncio.TIPO_ID')
                 ->get();
        return $query2->result();
    }

    public function dis_anuncios() {
        
        $query2 = $this->db->select('*')
                ->select('tipoanuncio.TIPO_DESC as tipo')
                ->from('anuncios')
                ->join('tipoanuncio', 'anuncios.TIPO_ANUNCIO = tipoanuncio.TIPO_ID')
                ->group_by('anuncios.TIPO_ANUNCIO')
                ->get();
        return $query2->result();
        
    }

    public function showme($pro_name) {

        $query = $this->db->get_where('anuncios', array('TIPO_ANUNCIO' => $pro_name));
        return $query->result();
    }

    public function find($pro_id) {//للبحث عن رقم المنتج وتحقق من وجوده 
        //this is for find record id->product
        $code = $this->db->where('ANUNCIO_ID', $pro_id)
                ->limit(1)
                ->get('anuncios');
        if ($code->num_rows() > 0) {
            return $code->row();
        } else {
            return array();
        }//end if code->num_rows > 0 
    }

    public function create($data_anuncios) {//لانشاء منتج جديد
        //guery insert into database 	
        $this->db->insert('anuncios', $data_anuncios);
    }

    public function edit($pro_id, $data_anuncios) {//للتعديل على المنتج
        //guery update FROM .. WHERE id->anuncios
        $this->db->where('ANUNCIO_ID', $pro_id)
                ->update('anuncios', $data_anuncios);
    }

    public function delete($pro_id) {
        //guery delete id->anuncios
        $this->db->where('ANUNCIO_ID', $pro_id)
                ->delete('anuncios');
    }

    public function report($report_anuncios) {

        $this->db->insert('relato', $report_anuncios);
    }

//end function craete

    public function reports() {
        $report = $this->db->get('relato');
        if ($report->num_rows() > 0) {
            return $report->result();
        } else {
            return array();
        } //end if num_rows
    }

//end function report

    public function del_report($rep_id_product) {
        $this->db->where('RELATO_ID_PRODUTO', $rep_id_product)
                ->delete('relato');
    }

}

//end class Model_anuncios
///////////////////////////////  Model_anuncios : this is use in controller admin/anuncios + home 