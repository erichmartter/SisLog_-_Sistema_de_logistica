<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_depositos extends CI_Model {

    public function all_depositos() {
      
        $query2 = $this->db->select('*')
                ->select('tipodeposito.TIPO_DESC as tipo')
                ->from('depositos')
                ->join('tipodeposito', 'depositos.TIPO_DEPOSITO = tipodeposito.TIPO_ID')
                 ->get();
        return $query2->result();
    }

    public function dis_depositos() {
        
        $query2 = $this->db->select('*')
                ->select('tipodeposito.TIPO_DESC as tipo')
                ->from('depositos')
                ->join('tipodeposito', 'depositos.TIPO_DEPOSITO = tipodeposito.TIPO_ID')
                ->group_by('depositos.TIPO_DEPOSITO')
                ->get();
        return $query2->result();
        
    }

    public function showme($pro_name) {

        $query = $this->db->get_where('depositos', array('TIPO_DEPOSITO' => $pro_name));
        return $query->result();
    }

    public function find($pro_id) {//للبحث عن رقم المنتج وتحقق من وجوده 
        //this is for find record id->product
        $code = $this->db->where('DEPOSITO_ID', $pro_id)
                ->limit(1)
                ->get('depositos');
        if ($code->num_rows() > 0) {
            return $code->row();
        } else {
            return array();
        }//end if code->num_rows > 0 
    }

    public function create($data_depositos) {//لانشاء منتج جديد
        //guery insert into database 	
        $this->db->insert('depositos', $data_depositos);
    }

    public function edit($pro_id, $data_depositos) {//للتعديل على المنتج
        //guery update FROM .. WHERE id->depositos
        $this->db->where('DEPOSITO_ID', $pro_id)
                ->update('depositos', $data_depositos);
    }

    public function delete($pro_id) {
        //guery delete id->depositos
        $this->db->where('DEPOSITO_ID', $pro_id)
                ->delete('depositos');
    }

    public function report($report_depositos) {

        $this->db->insert('relato', $report_depositos);
    }

//end function craete

    public function reports() {
        $report = $this->db->get('relato');
        if ($report->num_rows() > 0) {
            return $report->result();
        } else {
            return array();
        } //end if num_rows
    }

//end function report

    public function del_report($rep_id_product) {
        $this->db->where('RELATO_ID_PRODUTO', $rep_id_product)
                ->delete('relato');
    }

}

//end class Model_depositos
///////////////////////////////  Model_depositos : this is use in controller admin/depositos + home 