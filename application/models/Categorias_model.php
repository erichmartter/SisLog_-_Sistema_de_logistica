<?php

class Categorias_model extends CI_Model {

    // retorna os registros cadastrados na tabela categorias
    public function select() {
        $query = $this->db->get('categorias');
        return $query->result();
    }
    
    
    public function insert($categoria) {
        return $this->db->insert('categorias', $categoria);
    }
    
    // recupera o registro a ser alterado
    public function find($id) {
        $sql = "select * from categorias where CATEGORIA_ID = $id";
        // row: adequado para um único registro ($row)
        return $this->db->query($sql)->row();
    }
    
    public function update($categoria) {
        // cláusula where do update
        $this->db->where('CATEGORIA_ID', $categoria['CATEGORIA_ID']);
        // altera os dados
        return $this->db->update('categorias', $categoria);        
    }

    public function delete($id) {
        // cláusula where do delete
        $this->db->where('CATEGORIA_ID', $id);
        // altera os dados
        return $this->db->delete('categorias');        
    }
}