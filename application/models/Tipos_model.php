<?php

class Tipos_model extends CI_Model {

    // retorna os registros cadastrados na tabela tipoanuncio
    public function select() {
        $query = $this->db->get('tipoanuncio');
        return $query->result();
    }
    
    
    public function insert($tipo) {
        return $this->db->insert('tipoanuncio', $tipo);
    }
    
    // recupera o registro a ser alterado
    public function find($id) {
        $sql = "select * from tipoanuncio where TIPO_ID = $id";
        // row: adequado para um único registro ($row)
        return $this->db->query($sql)->row();
    }
    
    public function update($tipo) {
        // cláusula where do update
        $this->db->where('TIPO_ID', $tipo['DESC_MARCA']);
        // altera os dados
        return $this->db->update('tipoanuncio', $tipo);        
    }

    public function delete($id) {
        // cláusula where do delete
        $this->db->where('TIPO_ID', $id);
        // altera os dados
        return $this->db->delete('tipoanuncio');        
    }
}