<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_configuracoes extends CI_Model {
	
	
		/////////////////////////////////
		public function nome_site_config()
		{
			$query = $this->db->query('SELECT  * FROM configs WHERE NOME_CONFIG = "Site_nome"');
			return $query->result();
		}
		public function m_edit_nome_site($edit_nome_site)
		{
				$siteNome = 'Site_nome';
				$this->db->where('NOME_CONFIG',$siteNome)
					->update('configs',$edit_nome_site);
		}
		/////////////////////////////////
		public function footer_config()
		{
			$query = $this->db->query('SELECT  * FROM configs WHERE NOME_CONFIG = "Rodapé"');
			return $query->result();
		}
		public function m_edit_footer_settings($edit_footer)
		{
				$footer = 'Rodapé';
				$this->db->where('NOME_CONFIG',$footer)
					->update('configs',$edit_footer);
		}
		
	
		
		
		

}