<?php

class Marcas_model extends CI_Model {

    // retorna os registros cadastrados na tabela marcas
    public function select() {
        $query = $this->db->get('marcas');
        return $query->result();
    }
    
    
    public function insert($marca) {
        return $this->db->insert('marcas', $marca);
    }
    
    // recupera o registro a ser alterado
    public function find($id) {
        $sql = "select * from marcas where ID_MARCA = $id";
        // row: adequado para um único registro ($row)
        return $this->db->query($sql)->row();
    }
    
    public function update($marca) {
        // cláusula where do update
        $this->db->where('ID_MARCA', $marca['DESC_MARCA']);
        // altera os dados
        return $this->db->update('marcas', $marca);        
    }

    public function delete($id) {
        // cláusula where do delete
        $this->db->where('ID_MARCA', $id);
        // altera os dados
        return $this->db->delete('marcas');        
    }
}