<?php

class Produtos_model extends CI_Model {

    // retorna os registros cadastrados na tabela produtos
    public function select() {
        $query = $this->db->get('produtos');
        return $query->result();
    }
    
    
    public function insert($produto) {
        return $this->db->insert('produtos', $produto);
    }
    
    // recupera o registro a ser alterado
    public function find($id) {
        $sql = "select * from produtos where id = $id";
        // row: adequado para um único registro ($row)
        return $this->db->query($sql)->row();
    }
    
    public function update($produto) {
        // cláusula where do update
        $this->db->where('id', $produto['id']);
        // altera os dados
        return $this->db->update('produtos', $produto);        
    }

    public function delete($id) {
        // cláusula where do delete
        $this->db->where('id', $id);
        // altera os dados
        return $this->db->delete('produtos');        
    }
}