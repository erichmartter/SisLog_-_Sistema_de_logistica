<?php

class Veiculos_model extends CI_Model {

    public function sel() {
        $query2 = $this->db->select('*')
                ->select('marcas.DESC_MARCA as marca')
                ->from('veiculos')
                ->join('marcas', 'veiculos.MARCA = marcas.ID_MARCA')
                ->order_by('marcas.ID_MARCA')
                ->get();
        return $query2->result();
    }

    // retorna os registros cadastrados na tabela marcas
    public function sel2() {
        $query = $this->db->get('veiculos');
        $query1 = $this->db->get('marcas');
        $query2 = $this->db->select('*')
                ->select('marcas.DESC_MARCA as MARCA')
                ->from('veiculos')
                ->join('marcas', 'veiculos.MARCA = marcas.ID_MARCA')
                ->order_by('veiculos.DESC_VEICULO')
                ->get();
        //$sql = "select v.*, m.nome as marca from veiculos v inner join marcas m on v.marca_id = m.id order by v.id";
        // result: adequado para uma lista de dados (foreach)
        //return $this->db->query($sql)->result();
        return $query2->result();
    }

    public function sel1() {
        $query = $this->db->get('veiculos');
        return $query->result();
    }

    public function insert($veiculo) {
        return $this->db->insert('veiculos', $veiculo);
    }

    // recupera o registro a ser alterado
    public function find($id) {
        $sql = "select * from veiculos where ID_VEICULO = $id";
        // row: adequado para um único registro ($row)
        return $this->db->query($sql)->row();
    }

    public function update($veiculo) {
        // cláusula where do update
        $this->db->where('ID_VEICULO', $veiculo['ID_VEICULO']);
        // altera os dados
        return $this->db->update('veiculos', $veiculo);
    }

    public function delete($id) {
        // cláusula where do delete
        $this->db->where('ID_VEICULO', $id);
        // altera os dados
        return $this->db->delete('veiculos');
    }

}
