<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_produtos extends CI_Model {

    public function all_products() {
        $show = $this->db->get('produtos');
        if ($show->num_rows() > 0) {
            return $show->result();
        } else {
            return array();
        } //end if num_rows
    }

    public function dis_products() {
        $this->db->distinct();
        $query = $this->db->query('SELECT DISTINCT CATEGORIA FROM produtos');
        return $query->result();
    }

    public function showme($pro_name) {

        $query = $this->db->get_where('produtos', array('CATEGORIA' => $pro_name));
        return $query->result();
    }

    public function find($pro_id) {//للبحث عن رقم المنتج وتحقق من وجوده 
        //this is for find record id->product
        $code = $this->db->where('PRODUTO_ID', $pro_id)
                ->limit(1)
                ->get('produtos');
        if ($code->num_rows() > 0) {
            return $code->row();
        } else {
            return array();
        }//end if code->num_rows > 0 
    }

    public function create($data_products) {//لانشاء منتج جديد
        //guery insert into database 	
        $this->db->insert('produtos', $data_products);
    }

    public function edit($pro_id, $data_products) {//للتعديل على المنتج
        //guery update FROM .. WHERE id->products
        $this->db->where('PRODUTO_ID', $pro_id)
                ->update('produtos', $data_products);
    }

    public function delete($pro_id) {
        //guery delete id->products
        $this->db->where('PRODUTO_ID', $pro_id)
                ->delete('produtos');
    }

    public function report($report_products) {

        $this->db->insert('relato', $report_products);
    }

//end function craete

    public function reports() {
        $report = $this->db->get('relato');
        if ($report->num_rows() > 0) {
            return $report->result();
        } else {
            return array();
        } //end if num_rows
    }

//end function report

    public function del_report($rep_id_product) {
        $this->db->where('RELATO_ID_PRODUTO', $rep_id_product)
                ->delete('relato');
    }

}

//end class Model_products
///////////////////////////////  Model_products : this is use in controller admin/products + home 