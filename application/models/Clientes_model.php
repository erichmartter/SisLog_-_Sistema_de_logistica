<?php

class Clientes_model extends CI_Model {

    // retorna os registros cadastrados na tabela clientes
    public function select() {
        $query = $this->db->get('pessoa');
        return $query->result();
    }
    
    
    public function insert($cliente) {
        return $this->db->insert('pessoa', $cliente);
    }
    
    // recupera o registro a ser alterado
    public function find($id) {
        $sql = "select * from pessoa where PESSOA_ID = $id";
        // row: adequado para um único registro ($row)
        return $this->db->query($sql)->row();
    }
    
    public function update($cliente) {
        // cláusula where do update
        $this->db->where('PESSOA_ID', $cliente['PESSOA_ID']);
        // altera os dados
        return $this->db->update('pessoa', $cliente);        
    }

    public function delete($id) {
        // cláusula where do delete
        $this->db->where('PESSOA_ID', $id);
        // altera os dados
        return $this->db->delete('clientes');        
    }
}