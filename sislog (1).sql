-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 10-Out-2017 às 10:26
-- Versão do servidor: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sislog`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `anuncios`
--

CREATE TABLE `anuncios` (
  `ANUNCIO_ID` int(11) NOT NULL,
  `TIPO_ANUNCIO` int(3) NOT NULL,
  `DESCRICAO` varchar(200) NOT NULL,
  `ALTURA` decimal(10,1) NOT NULL,
  `LARGURA` decimal(10,1) NOT NULL,
  `PROFUNDIDADE` decimal(10,1) NOT NULL,
  `PESO` int(11) NOT NULL,
  `NOME` varchar(60) NOT NULL,
  `ORIGEM` varchar(100) NOT NULL,
  `DESTINO` varchar(100) NOT NULL,
  `PRAZO_ENTREGA` date NOT NULL,
  `ANUNCIO_IMAGEM` text,
  `VALOR` float(9,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `anuncios`
--

INSERT INTO `anuncios` (`ANUNCIO_ID`, `TIPO_ANUNCIO`, `DESCRICAO`, `ALTURA`, `LARGURA`, `PROFUNDIDADE`, `PESO`, `NOME`, `ORIGEM`, `DESTINO`, `PRAZO_ENTREGA`, `ANUNCIO_IMAGEM`, `VALOR`) VALUES
(1, 1, 'Anunciando...', '20.0', '50.0', '65.0', 15, 'Opa', 'tal', 'tal outro lugar', '2017-10-04', 'images.jpg', 10000.00),
(2, 1, 'Teste', '20.0', '50.0', '65.0', 15, 'Opa', 'tal', 'tal outro lugar', '2017-10-04', 'images.jpg', 10.50),
(3, 3, 'Levar para RG', '10.0', '20.0', '88.0', 25, 'Palets', 'Pelotas', 'Rio Grande', '2017-10-18', 'images.jpg', NULL),
(4, 3, 'Levar para RG', '10.0', '20.0', '88.0', 25, 'Palets', 'Pelotas', 'Rio Grande', '2017-10-18', 'images.jpg', NULL),
(5, 2, 'descrição do anuncio', '89.0', '56.0', '12.0', 0, '', '', '', '0000-00-00', 'Dell_Computer.jpg', NULL),
(6, 2, 'descrição do anuncio', '89.0', '56.0', '12.0', 23, 'Opa', 'Pel', 'RG', '0000-00-00', 'carga.png', NULL),
(7, 3, 'Vai', '12.0', '23.0', '56.0', 78, 'Anuncio Olá', 'SP', 'MG', '2017-10-24', 'carga.png', 20000.00),
(8, 3, 'Vai', '12.0', '23.0', '56.0', 78, 'Anuncio Olá', 'SP', 'MG', '2017-10-24', 'carga.png', 20000.00),
(9, 1, 'sqwdw', '0.0', '0.0', '0.0', 0, 'Oi', '', '', '2017-10-11', 'zorte.png', 1000.00),
(10, 1, 'r4334', '0.0', '0.0', '0.0', 0, 'Oi', '', '', '2017-10-04', 'veiculos.png', 89859.00),
(11, 1, 'dsds', '0.0', '0.0', '0.0', 0, 'Claro', '', '', '2017-10-05', 'board1.jpg', 89859.00);

-- --------------------------------------------------------

--
-- Estrutura da tabela `categorias`
--

CREATE TABLE `categorias` (
  `CATEGORIA_ID` int(11) NOT NULL,
  `DESC_CATEGORIA` varchar(250) NOT NULL,
  `SETOR` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `categorias`
--

INSERT INTO `categorias` (`CATEGORIA_ID`, `DESC_CATEGORIA`, `SETOR`) VALUES
(1, 'Eletrônicos', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cidade`
--

CREATE TABLE `cidade` (
  `ID_CIDADE` int(11) NOT NULL,
  `NOME_CIDADE` varchar(45) DEFAULT NULL,
  `UF_ID_UF` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `cidade`
--

INSERT INTO `cidade` (`ID_CIDADE`, `NOME_CIDADE`, `UF_ID_UF`) VALUES
(1, 'Pelotas', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `configs`
--

CREATE TABLE `configs` (
  `CONFIG_ID` int(3) NOT NULL,
  `NOME_CONFIG` varchar(60) NOT NULL,
  `VALORES` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `configs`
--

INSERT INTO `configs` (`CONFIG_ID`, `NOME_CONFIG`, `VALORES`) VALUES
(3, 'Rodapé', 'Todos os Direitos reservados 2017'),
(4, 'Site_nome', 'SisLog');

-- --------------------------------------------------------

--
-- Estrutura da tabela `faturas`
--

CREATE TABLE `faturas` (
  `FATURA_ID` int(16) NOT NULL,
  `DATA` datetime NOT NULL,
  `SEC_DATA` datetime NOT NULL,
  `USUARIO_ID` int(10) NOT NULL,
  `STATUS` enum('pago','confirmado','naoPago','cancelado','expirado') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `faturas`
--

INSERT INTO `faturas` (`FATURA_ID`, `DATA`, `SEC_DATA`, `USUARIO_ID`, `STATUS`) VALUES
(10001001, '2015-05-06 08:13:09', '2015-05-07 08:13:09', 2, ''),
(10001002, '2015-05-06 08:17:15', '2015-05-07 08:17:15', 3, '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `grupos`
--

CREATE TABLE `grupos` (
  `GRUPO_ID` tinyint(1) NOT NULL,
  `GRUPO_NOME` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `grupos`
--

INSERT INTO `grupos` (`GRUPO_ID`, `GRUPO_NOME`) VALUES
(1, 'admin'),
(2, 'c_admin'),
(3, 'membro');

-- --------------------------------------------------------

--
-- Estrutura da tabela `marcas`
--

CREATE TABLE `marcas` (
  `ID_MARCA` int(11) NOT NULL,
  `DESC_MARCA` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `marcas`
--

INSERT INTO `marcas` (`ID_MARCA`, `DESC_MARCA`) VALUES
(3, 'Volvo'),
(4, 'Ford'),
(5, 'Chevrolet');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos`
--

CREATE TABLE `produtos` (
  `PRODUTO_ID` int(16) NOT NULL,
  `CATEGORIA` varchar(50) NOT NULL,
  `PRODUTO_NOME` varchar(20) NOT NULL,
  `DESC_PRODUTO` text NOT NULL,
  `VALOR` int(9) NOT NULL,
  `PRODUTO_ESTOQUE` int(3) NOT NULL,
  `PRODUTO_IMAGEM` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `produtos`
--

INSERT INTO `produtos` (`PRODUTO_ID`, `CATEGORIA`, `PRODUTO_NOME`, `DESC_PRODUTO`, `VALOR`, `PRODUTO_ESTOQUE`, `PRODUTO_IMAGEM`) VALUES
(1, 'PC', 'Dell', 'Dell INSPIRON N5111\r\nRAM 2GB\r\nCORE i5\r\nAVG 1Gb\r\nCPU 3000', 25000, 3, 'Dell_Computer.jpg'),
(2, 'Laptop', 'Toshiba', 'RAM 1GB\r\nCORE i7\r\nAVG 2Gb\r\nCPU 3500dkods´ckdps´cpsdkpkcp´sdkp´cksdp´kcp´sdkp´kvwep´kvpékp´kdsp´vkp´sdkpv´sdkp´vksdp´kvpsdkvp´sdkp´kwpefko´wejoewjpifjweiweojfiwe', 50000, 5, 'prod_satA205-OFTWH_300-01.jpg'),
(3, 'PC', 'HP', 'HP 300 \r\nram 2 gb\r\navg 2\r\ncpu 3500\r\ndvd\r\ncam 16 px\r\n', 75000, 1, 'images.jpg'),
(4, 'Mobile', 'HTC sensation XL', 'htc', 45000, 1, 'htc_sensation_xl_28.jpg'),
(5, 'Mobile', 'Iphone 6', 'Iphone 6', 46000, 1, 'aabffb1c6425f95fd26db8595ee28c0e_png.jpg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `relato`
--

CREATE TABLE `relato` (
  `RELATO_ID` int(9) NOT NULL,
  `RELATO_NOME` varchar(60) NOT NULL,
  `RELATO_ID_PRODUTO` int(9) NOT NULL,
  `RELATO_NOME_PRODUTO` varchar(60) NOT NULL,
  `RELATO_NOME_USUARIO` varchar(60) NOT NULL,
  `RELATO_USUARIO_GRUPO` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `relato`
--

INSERT INTO `relato` (`RELATO_ID`, `RELATO_NOME`, `RELATO_ID_PRODUTO`, `RELATO_NOME_PRODUTO`, `RELATO_NOME_USUARIO`, `RELATO_USUARIO_GRUPO`) VALUES
(3, 'PC', 1, 'Dell', '', '0'),
(4, 'Laptop', 2, 'Toshiba', '', '0'),
(5, 'Mobile', 4, 'HTC sensation XL', 'test', '3'),
(6, 'Laptop', 2, 'Toshiba', 'test', '3'),
(7, 'PC', 3, 'HP', 'Gost', 'Gost'),
(8, 'PC', 1, 'Dell', 'hichamtest', '3');

-- --------------------------------------------------------

--
-- Estrutura da tabela `secoes`
--

CREATE TABLE `secoes` (
  `SECAO_ID` varchar(40) NOT NULL,
  `ENDERECO_IP` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `DATA` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `secoes`
--

INSERT INTO `secoes` (`SECAO_ID`, `ENDERECO_IP`, `timestamp`, `DATA`) VALUES
('11b433b91da91fd2df474c65209d5bdf04f52391', '::1', 1430886198, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433303838353930363b757365726e616d657c733a363a2268696368616d223b67726f75707c733a313a2233223b),
('18e5031ed538645b4ccb810918bb6bb4def54f0f', '::1', 1430885736, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433303838353630353b757365726e616d657c733a363a2268696368616d223b67726f75707c733a313a2233223b),
('216782d346ecb725467fa1a08f49f3e057705ad8', '::1', 1430882453, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433303838323430303b757365726e616d657c733a363a2268696368616d223b67726f75707c733a313a2233223b),
('303d5ff594029d513c5b48c68edde9af03ed959f', '::1', 1430889412, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433303838393133383b757365726e616d657c733a363a2268696368616d223b67726f75707c733a313a2233223b),
('473886d26486392bbc947defc69cdcf66424de77', '::1', 1430886630, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433303838363433363b757365726e616d657c733a363a2268696368616d223b67726f75707c733a313a2233223b),
('75026a28ee50856686459e50bbf04e02fbdbe1b7', '::1', 1430887393, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433303838373136333b757365726e616d657c733a363a2268696368616d223b67726f75707c733a313a2233223b),
('8f1b75f2af02d572e0b06950a4286efa36064e28', '::1', 1430892337, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433303839323333353b),
('8f5d8f2b27c17d06a261c2d12b9c6783791f23b0', '::1', 1430888044, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433303838373836323b757365726e616d657c733a363a2268696368616d223b67726f75707c733a313a2233223b),
('a460a08a293ae491ad52e0381efaa0070ec01c76', '::1', 1430888288, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433303838383137303b757365726e616d657c733a363a2268696368616d223b67726f75707c733a313a2233223b),
('ac03e0e37872a413802709d8289adcfe1d6574db', '::1', 1430887766, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433303838373437343b757365726e616d657c733a363a2268696368616d223b67726f75707c733a313a2233223b),
('c64d3fd803821b27ddca58c1de092623cc89ee8a', '::1', 1430890441, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433303839303334343b757365726e616d657c733a363a2268696368616d223b67726f75707c733a313a2233223b6d6573736167657c733a35383a225468616e6b20796f75202e2e2e2e2e2077652077696c6c20636865636b206f6e20796f7572207061796d656e7420636f6e6669726d6174696f6e223b5f5f63695f766172737c613a313a7b733a373a226d657373616765223b733a333a226f6c64223b7d),
('d03788541a1ed30cb5f5fb7acc025264b50bf1fe', '::1', 1430888790, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433303838383532303b757365726e616d657c733a363a2268696368616d223b67726f75707c733a313a2233223b),
('d7a7a2250ad798163a90ba09c3e0501aa17bbf2b', '::1', 1430883463, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433303838333230313b757365726e616d657c733a363a2268696368616d223b67726f75707c733a313a2233223b),
('e5a96ef2ea1800a7dc0a794c1fc53ebbd742aa3e', '::1', 1430893426, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433303839333432363b),
('e6528b002d7aacfeaa0d5e91ca1b9e3d714d5487', '::1', 1430891019, 0x5f5f63695f6c6173745f726567656e65726174657c693a313433303839303733343b757365726e616d657c733a363a2268696368616d223b67726f75707c733a313a2233223b);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipoanuncio`
--

CREATE TABLE `tipoanuncio` (
  `TIPO_ID` int(11) NOT NULL,
  `TIPO_DESC` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tipoanuncio`
--

INSERT INTO `tipoanuncio` (`TIPO_ID`, `TIPO_DESC`) VALUES
(1, 'Armazenagem'),
(2, 'Transporte'),
(3, 'Armazenagem e Transporte');

-- --------------------------------------------------------

--
-- Estrutura da tabela `uf`
--

CREATE TABLE `uf` (
  `ID_UF` int(11) NOT NULL,
  `NOME_UF` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `uf`
--

INSERT INTO `uf` (`ID_UF`, `NOME_UF`) VALUES
(1, 'RS');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `USUARIO_ID` int(10) NOT NULL,
  `USUARIO_NOME` varchar(25) NOT NULL,
  `USUARIO_SENHA` varchar(60) NOT NULL,
  `USUARIO_GRUPO` tinyint(1) NOT NULL,
  `STATUS` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`USUARIO_ID`, `USUARIO_NOME`, `USUARIO_SENHA`, `USUARIO_GRUPO`, `STATUS`) VALUES
(1, 'admin', 'admin123', 1, 1),
(2, 'hicham', 'admin123', 3, 1),
(3, 'dyaa', 'admin123', 3, 1),
(4, 'admin1', 'admin123', 2, 1),
(5, 'OpaBino', 'admin123', 3, 1),
(6, 'Testando', 'admin123', 3, 1),
(7, 'Billy', 'admin123', 3, 1),
(8, 'Jack', 'admin123', 3, 1),
(9, 'Beltrano1', 'admin123', 1, 1),
(10, 'Beltrano2', 'admin123', 1, 1),
(11, 'Beltrano5', 'admin123', 3, 1),
(12, 'Beltrano8', 'admin123', 3, 1),
(13, 'Beltrano7', 'admin123', 3, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `veiculos`
--

CREATE TABLE `veiculos` (
  `ID_VEICULO` int(11) NOT NULL,
  `DESC_VEICULO` varchar(250) NOT NULL,
  `PLACA` varchar(10) NOT NULL,
  `MARCA` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `veiculos`
--

INSERT INTO `veiculos` (`ID_VEICULO`, `DESC_VEICULO`, `PLACA`, `MARCA`) VALUES
(1, 'Truck-One', 'uuu-5555', 3),
(3, 'F-1000', 'iii-0000', 4),
(4, 'B250', 'iii-0000', 3),
(5, 'F-250', 'uik-0258', 4),
(6, 'Van', 'HBU-5577', 3),
(8, 'Van', 'Ids-9944', 4);

-- --------------------------------------------------------

--
-- Estrutura da tabela `vendas`
--

CREATE TABLE `vendas` (
  `VENDA_ID` int(16) NOT NULL,
  `FATURA_ID` int(16) NOT NULL,
  `PRODUTO_ID` int(16) NOT NULL,
  `CATEGORIA_PRODUTO` varchar(60) NOT NULL,
  `PRODUTO_DESC` varchar(60) NOT NULL,
  `QUANTIDADE` int(3) NOT NULL,
  `VALOR` int(9) NOT NULL,
  `OPCOES` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `vendas`
--

INSERT INTO `vendas` (`VENDA_ID`, `FATURA_ID`, `PRODUTO_ID`, `CATEGORIA_PRODUTO`, `PRODUTO_DESC`, `QUANTIDADE`, `VALOR`, `OPCOES`) VALUES
(10001001, 10001001, 1, 'PC', 'Dell', 1, 25000, ''),
(10001002, 10001002, 5, 'Mobile', 'Iphone 6', 1, 46000, '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `anuncios`
--
ALTER TABLE `anuncios`
  ADD PRIMARY KEY (`ANUNCIO_ID`);

--
-- Indexes for table `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`CATEGORIA_ID`);

--
-- Indexes for table `cidade`
--
ALTER TABLE `cidade`
  ADD PRIMARY KEY (`ID_CIDADE`,`UF_ID_UF`),
  ADD UNIQUE KEY `ID_CIDADE_UNIQUE` (`ID_CIDADE`),
  ADD KEY `fk_CIDADE_UF1_idx` (`UF_ID_UF`);

--
-- Indexes for table `configs`
--
ALTER TABLE `configs`
  ADD PRIMARY KEY (`CONFIG_ID`);

--
-- Indexes for table `faturas`
--
ALTER TABLE `faturas`
  ADD PRIMARY KEY (`FATURA_ID`);

--
-- Indexes for table `grupos`
--
ALTER TABLE `grupos`
  ADD PRIMARY KEY (`GRUPO_ID`);

--
-- Indexes for table `marcas`
--
ALTER TABLE `marcas`
  ADD PRIMARY KEY (`ID_MARCA`);

--
-- Indexes for table `produtos`
--
ALTER TABLE `produtos`
  ADD PRIMARY KEY (`PRODUTO_ID`);

--
-- Indexes for table `relato`
--
ALTER TABLE `relato`
  ADD PRIMARY KEY (`RELATO_ID`);

--
-- Indexes for table `secoes`
--
ALTER TABLE `secoes`
  ADD PRIMARY KEY (`SECAO_ID`),
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `tipoanuncio`
--
ALTER TABLE `tipoanuncio`
  ADD PRIMARY KEY (`TIPO_ID`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`USUARIO_ID`);

--
-- Indexes for table `veiculos`
--
ALTER TABLE `veiculos`
  ADD PRIMARY KEY (`ID_VEICULO`),
  ADD KEY `MARCA` (`MARCA`);

--
-- Indexes for table `vendas`
--
ALTER TABLE `vendas`
  ADD PRIMARY KEY (`VENDA_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `anuncios`
--
ALTER TABLE `anuncios`
  MODIFY `ANUNCIO_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `categorias`
--
ALTER TABLE `categorias`
  MODIFY `CATEGORIA_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cidade`
--
ALTER TABLE `cidade`
  MODIFY `ID_CIDADE` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `configs`
--
ALTER TABLE `configs`
  MODIFY `CONFIG_ID` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `faturas`
--
ALTER TABLE `faturas`
  MODIFY `FATURA_ID` int(16) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10001003;
--
-- AUTO_INCREMENT for table `grupos`
--
ALTER TABLE `grupos`
  MODIFY `GRUPO_ID` tinyint(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `marcas`
--
ALTER TABLE `marcas`
  MODIFY `ID_MARCA` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `produtos`
--
ALTER TABLE `produtos`
  MODIFY `PRODUTO_ID` int(16) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `relato`
--
ALTER TABLE `relato`
  MODIFY `RELATO_ID` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tipoanuncio`
--
ALTER TABLE `tipoanuncio`
  MODIFY `TIPO_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `USUARIO_ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `veiculos`
--
ALTER TABLE `veiculos`
  MODIFY `ID_VEICULO` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `vendas`
--
ALTER TABLE `vendas`
  MODIFY `VENDA_ID` int(16) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10001003;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `veiculos`
--
ALTER TABLE `veiculos`
  ADD CONSTRAINT `veiculos_ibfk_1` FOREIGN KEY (`MARCA`) REFERENCES `marcas` (`ID_MARCA`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
