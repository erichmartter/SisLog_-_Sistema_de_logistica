<header class="main-header">

    <?php if ($this->session->userdata('grupo') == '1' or $this->session->userdata('grupo') == '2'): ?>
        <?php include 'includes/inc_menuLateral.php'; ?>
    <?php endif; ?>


    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav navbar-left">
                <li>
                    <a href="<?php echo base_url(); ?>">Home</a>
                </li>
                <li>
                    <a href="">About</a>
                </li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <?php if ($this->session->userdata('grupo') == '1' or $this->session->userdata('grupo') == '2'): ?>
                    <li>
                        <?php echo anchor('admin/invoices', 'Invoices List'); ?>
                    </li>
                    <li>
                        <?php echo anchor('admin/products/reports', 'Product Report'); ?>
                    </li>
                    <li>
                        <?php echo anchor('admin/products', 'Dashboard'); ?>
                    <?php endif; ?>
                </li>
                <?php if ($this->session->userdata('grupo') == '3'): ?>
                    <li>
                        <?php echo anchor('customer/payment_confirmation', 'Payment Confirmation'); ?>
                    </li>
                    <li>
                        <?php echo anchor('customer/shopping_history', 'History'); ?>
                    </li>
                <?php endif; ?>

                <?php if ($this->session->userdata('nome')): ?>
                    <li>

                        <?php echo ('<a>' . 'Bem vindo, ' . $this->session->userdata('nome') . '</a>'); ?>
                    </li>

                    <li>
                        <?php echo anchor('login/logout', 'Logout'); ?>
                    <?php else: ?>
                    </li>
                    <li>
                        <?php echo anchor('login', 'Login / Sign Up'); ?>
                    <?php endif; ?>
                    </li>							
                <?php if ($this->session->userdata('grupo') != '1' and $this->session->userdata('grupo') != '2'): ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"> My Cart <?= $this->cart->total_items(); ?> <i class="fa fa-shopping-cart"> </i> <b class="caret"> </b></a>
                        <ul class="dropdown-menu">

                            <li>
                                <?php
                                $url_cart = 'My cart ';
                                $url_cart .= $this->cart->total_items() . ' <i class="fa fa-shopping-cart"></i></a>';
                                ?>
                                <?= anchor('home/cart', $url_cart); ?>

                            </li>
                            <li>
                                <?php
                                $url_order = 'Check Out';
                                $url_order .= ' <i class="fa fa-cc-paypal"></i> ' . ' <i class="fa fa-credit-card"></i> ' . ' <i class="fa fa-cc-visa"></i></a> ';
                                ?>
                                <?php if ($this->cart->total_items() != 0): ?>
                                    <?= anchor('fatura', $url_order); ?>
                                <?php else: ?>
                                    <?= anchor(base_url(), $url_order); ?>
                                <?php endif; ?>
                            </li>
                        </ul>
                    </li>


                <?php endif; ?>



            </ul>
        </div>
    </nav>
</header>