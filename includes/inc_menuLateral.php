
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Pesquisar...">
                <span class="input-group-btn">
                    <button type="submit" name="pesquisar" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <!--menu clientes-->
            <li class="treeview">
                <a href="assets/#">
                    <i class="ion ion-person"></i>
                    <span>Clientes</span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url('membros/members') ?>"><i class="fa fa-circle-o"></i> Ver Clientes</a></li>
                </ul>
            </li>
            <!--menu veículos-->
            <li class="treeview">
                <a href="assets/#">
                    <i class="fa fa-truck"></i>
                    <span>Veículos</span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url('veiculos');?> "><i class="fa fa-circle-o"></i> Ver veículos</a></li>
                    <li><a href="<?php echo base_url('marcas');?>"><i class="fa fa-circle-o"></i> Adicionar Marca</a></li>
                    
                </ul>
            </li>
            <!--menu produtos-->
            <li class="treeview">
                <a href="assets/#">
                    <i class="fa fa-cubes"></i>
                    <span>Produtos</span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url('produtos');?>"><i class="fa fa-circle-o"></i> Ver produtos</a></li>
                    <li><a href="<?php echo base_url('categorias');?>"><i class="fa fa-circle-o"></i> Ver categorias</a></li>
                </ul>
            </li>
            <!--menu anuncios-->
            <li class="treeview">
                <a href="assets/#">
                    <i class="fa fa-cubes"></i>
                    <span>Anúncios</span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url('anuncios');?>"><i class="fa fa-circle-o"></i> Ver anúncios</a></li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>