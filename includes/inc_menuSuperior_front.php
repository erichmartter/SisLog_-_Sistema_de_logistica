
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="<?php echo base_url(); ?>"><?php foreach ($get_sitename as $sitename): ?><?= $sitename->VALORES; ?><?php endforeach; ?></a>

        </div>
        <ul class="nav navbar-nav">
                             <li><?php echo anchor('home', 'Contato'); ?></li>
								
            <?php if ($this->session->userdata('nome')): ?>
                 <li><?php echo anchor('anuncios/criar', 'Criar anúncio'); ?></li>
            <?php endif; ?>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <?php if ($this->session->userdata('nome')): ?>
                <li><?php echo ('<a>' . 'Bem vindo, ' . $this->session->userdata('nome') . '</a>'); ?></li>
                <li><?php echo anchor('login/logout', 'Logout'); ?><?php else: ?></li>
                <li><?php echo anchor('login', 'Login'); ?></li><?php endif; ?>
        </ul>
    </div>
</nav>